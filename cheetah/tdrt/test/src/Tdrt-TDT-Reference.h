/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef TDRT_TDT_REFERENCE_H_
#define TDRT_TDT_REFERENCE_H_

#include "cheetah/tdrt/TimeSeries.h"
#include <tuple>

namespace ska {
namespace cheetah {
namespace tdrt {
namespace test {

/**
 * @brief
 * Time Domain Resampler Reference Model
 *
 * @details
 */
class TdrtTDTReference
{
    public:
	TdrtTDTReference();

    void process(TimeSeries<float>& h_input,
                 TimeSeries<float>& h_output,
                 float acceleration); // m/s/s
	
    
    // Data generators:
    
    // Simulates a pulsar with a constant acceleration. 
    // Each simulated pulse is one-bin wide and has an amplitude =1.
    // More often than not, I new pulse does not land exactly in a time bin,
    // so the signal is distributed appropriately between two bins.

    void const_accel_pulsar(double acceleration, // m/s/s
                            float  period,       // s
                            TimeSeries<float>& h_output);

	// sine generator for calibration. period in sec.
	void sine_calibration_signal(float period, TimeSeries<float>& h_output);

	// Resampled period: 

    // Calculates the new period of the pulsar after resampling
    // Can be used to verify that resample functions working correctly
    double resampled_period(double acceleration, double period, double tsamp, int length);

    // Data checkers:

    // The simplest way to compare the two output vectors is to take the 
    // correlation (SUM(tsA[i]*tsB[i]). In this function I normalize the 
    // Correlation by the number of pulses found. Given the nature of the 
    // input vectors, this will yield a correlation = 1 when they're identical.
    // One "feature" to this test is it will be highly sensitive to the pulses 
    // being off even by potentially one bin. It may or may not be too stringent.

    float tdrt_correlation(TimeSeries<float>& h_tsA, TimeSeries<float>& h_tsB);

    
    // This function moves through each vector, records the location of each 
    // pulse, finds the difference pulse-wise between the two vectors, sums 
    // these differences, and normalizes by the number of pulses. In short, 
    // this is an mean difference in the locations of what should be nominally
    // the same pulse. In the ideal case this is zero. 
    
    float tdrt_bin_diff(TimeSeries<float>& h_tsA, TimeSeries<float>& h_tsB);

    // Calculates the mean recovered amplitude for each time series independently 
    // and then compare the results

    // Folds a time series with given period
    // NOTE: This will only work with integer periods
    // The function casts period to an int and folds accordingly    
    // It then find the bin with the max value and reports the sum of the
    // max bin and +/- adjacent bins from the max
    double fold_pulsar(TimeSeries<float>& h_ts, double period, int adjacent);

	std::tuple<float, size_t> tdrt_recover_amp(TimeSeries<float>& h_ts);
   
    void tdrt_bin_spacing(TimeSeries<float>& h_ts);

    private:
};

} // namespace test
} // namespace tdrt
} // namespace cheetah
} // namespace ska

#endif /* TDRT_TDT_REFERENCE_H_ */

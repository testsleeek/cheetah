/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/detail/SpsTask.h"


namespace ska {
namespace cheetah {
namespace sps {


template<typename DmHandler, typename SpHandler, typename BufferType>
SpsTask<DmHandler, SpHandler, BufferType>::SpsTask(sps::Config const& config, DmHandler dm_handler, SpHandler sp_handler)
    : _algos(astroaccelerate::Sps(config))
    , _dm_handler(std::move(dm_handler))
    , _sp_handler(std::move(sp_handler))
{
}

template<typename DmHandler, typename SpHandler, typename BufferType>
SpsTask<DmHandler, SpHandler, BufferType>::~SpsTask()
{
}

template<typename DmHandler, typename SpHandler, typename BufferType>
template<typename Arch>
void SpsTask<DmHandler, SpHandler, BufferType>::operator()(panda::PoolResource<Arch>& resource, BufferType buffer)
{
    auto& algo = _algos.template get<Arch>();
    algo(resource, buffer, _dm_handler, _sp_handler);
}

template<typename DmHandler, typename SpHandler, typename BufferType>
template<typename DataType>
void SpsTask<DmHandler, SpHandler, BufferType>::call_dm_handler(DataType&& d) const
{
    return _dm_handler(std::forward<DataType>(d));
}

template<typename DmHandler, typename SpHandler, typename BufferType>
template<typename DataType>
void SpsTask<DmHandler, SpHandler, BufferType>::call_sp_handler(DataType&& d) const
{
    return _sp_handler(std::forward<DataType>(d));
}

template<typename DmHandler, typename SpHandler, typename BufferType>
template<typename Arch>
std::size_t SpsTask<DmHandler, SpHandler, BufferType>::buffer_overlap() const
{
    return _algos.get<Arch>().buffer_overlap();
}

template<typename DmHandler, typename SpHandler, typename BufferType>
template<typename... Args>
std::size_t SpsTask<DmHandler, SpHandler, BufferType>::set_dedispersion_strategy(Args&&... args)
{
    // TODO send to each algorithm and return the higest returned value
    return _algos.get<cheetah::Cuda>().set_dedispersion_strategy(std::forward<Args>(args)...);
}

} // namespace sps
} // namespace cheetah
} // namespace ska

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sps/Sps.h"
#include "panda/ResourcePool.h"
#include "panda/FinishTask.h"
#include "panda/Log.h"


namespace ska {
namespace cheetah {
namespace sps {


template<class ConfigType, typename NumericalRep>
Sps<ConfigType, NumericalRep>::Sps(ConfigType const& config, DmHandler dm_handler, SpHandler sp_handler)
    : _pool(config.pool())
    , _task_ptr(std::make_shared<TaskType>(config, std::move(dm_handler), std::move(sp_handler)))
    , _agg_buf_filler( [this](typename BufferFillerType::AggregationBufferType buffer)
                       {
#ifdef ENABLE_ASTROACCELERATE
                           _pool.submit(_task_ptr, std::move(buffer));
#else //ENABLE_ASTROACCELERATE
                           (void) buffer;
                           (void) this;
#endif //ENABLE_ASTROACCELERATE
                       }
                       ,0)
    , _current_number_of_channels(0)
{
    _dedisp_samples = config.dedispersion_samples();
}

template<class ConfigType, typename NumericalRep>
Sps<ConfigType, NumericalRep>::~Sps()
{
    // replace the callback with one that will indicate when we are finiahed
    panda::FinishTask<TaskType> finish(*_task_ptr);
     _agg_buf_filler.full_buffer_handler( [&](typename BufferFillerType::AggregationBufferType buffer)
                       {
#ifdef ENABLE_ASTROACCELERATE
                           auto job = _pool.submit(finish, std::move(buffer));
                           job->wait();
                           if(job->status() != ska::panda::ResourceJob::JobStatus::Finished )
                           {
                               finish.finished();
                           }
#else //ENABLE_ASTROACCELERATE
                           PANDA_LOG_WARN << "Sps has no implementation for this setup";
                           finish.finished();
                           (void) buffer;
#endif //ENABLE_ASTROACCELERATE
                       }
    );
    if(_agg_buf_filler.flush())
    {
       finish.wait();
    }
    PANDA_LOG_DEBUG << "~Sps() destructor";
}

template<class ConfigType, typename NumericalRep>
void Sps<ConfigType, NumericalRep>::operator()(TimeFrequencyType& data)
{
#ifdef ENABLE_ASTROACCELERATE
    if (data.number_of_channels() != _current_number_of_channels)
    {
        //Document this better!!
        _dedisp_samples = _task_ptr->set_dedispersion_strategy(panda::nvidia::min_gpu_memory(_pool), data);
        PANDA_LOG << "setting buffer size to " << _dedisp_samples << " samples";
        _agg_buf_filler.resize(data.number_of_channels() * _dedisp_samples);
        //Note agg buffer overlap is in number of data points, not time samples.
        auto overlap = _task_ptr->template buffer_overlap<cheetah::Cuda>();
        PANDA_LOG << "setting buffer overlap to " << overlap << " samples";
        _agg_buf_filler.set_overlap( overlap * data.number_of_channels() );
        _current_number_of_channels = data.number_of_channels();
    }

    _agg_buf_filler << data;
#else //ENABLE_ASTROACCELERATE
    (void) data;
    PANDA_LOG_WARN << "No available Sps algorithm";
    auto dm_data = DmTrialType::make_shared(data.start_time());
    _task_ptr->call_dm_handler(dm_data);
    auto sp_data = SpType::make_shared();
    _task_ptr->call_sp_handler(sp_data);
#endif //ENABLE_ASTROACCELERATE
}

} // namespace sps
} // namespace cheetah
} // namespace ska

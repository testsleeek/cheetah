include_directories(${GTEST_INCLUDE_DIR})

link_directories(${GTEST_LIBRARY_DIR})

set(
   gtest_fpga_src
    src/gtest_fpga.cpp
)
add_executable(gtest_fpga ${gtest_fpga_src} )
target_link_libraries(gtest_fpga ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fpga gtest_fpga --test_data "${CMAKE_CURRENT_LIST_DIR}/data")

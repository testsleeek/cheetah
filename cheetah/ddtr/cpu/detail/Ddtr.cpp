/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/ddtr/cpu/Ddtr.h"


namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {

template<typename PoolType, typename NumericalT>
Ddtr<PoolType, NumericalT>::Ddtr(ddtr::Config const& algo_config
               , DedispersionHandler& dm_trials_handler
               , PoolType& pool)
    : BaseT(algo_config.cpu_algo_config(), algo_config)
    , _dm_trials_handler(dm_trials_handler)
    , _pool(pool)
    , _agg_buffer_filler_ptr(new BufferFillerType([this](BufferType buffer){
        this->_pool.submit(*this, std::move(buffer));
        },0))
    , _first_call(true)
    , _max_delay(0)
    , _dedispersion_samples(0)
{
}

template<typename PoolType, typename NumericalT>
Ddtr<PoolType, NumericalT>::Ddtr(Ddtr&& other)
    : BaseT(std::move(other))
    , _dm_trials_handler(other._dm_trials_handler)
    , _pool(other._pool)
    , _agg_buffer_filler_ptr(std::move(other._agg_buffer_filler_ptr))
    , _first_call(other._first_call)
    , _max_delay(other._max_delay)
    , _dedispersion_samples(other._dedispersion_samples)
    , _dm_trial_metadata(std::move(other._dm_trial_metadata))
    , _dm_factors(std::move(other._dm_factors))
{
    _agg_buffer_filler_ptr->full_buffer_handler([this](BufferType buffer)
    {
        this->_pool.submit(*this, std::move(buffer));
    });
}

template<typename PoolType, typename NumericalT>
Ddtr<PoolType, NumericalT>::~Ddtr()
{
    _agg_buffer_filler_ptr.reset();
    PANDA_LOG << "agg buffer fill reset";
    _pool.wait();
}

template<typename PoolType, typename NumericalT>
void Ddtr<PoolType, NumericalT>::operator()(panda::PoolResource<cheetah::Cpu>& /*cpu*/
    , BufferType const& data)
{
    auto const& tf_obj = *(data.composition().front());
    std::size_t nchans = tf_obj.number_of_channels();
    std::size_t nsamples = data.data_size() / nchans;
    if  (data.data_size() % nchans != 0)
    {
        throw panda::Error("AggregationBuffer does not contain a whole number of channels");
    }
    if (data.data_size() < _max_delay * nchans)
    {
        PANDA_LOG_WARN << "AggregationBuffer is too small to be processed ("
                       << data.data_size() << " < " << _max_delay*nchans << ")\n"<<"Skipping Current Buffer";
        return;
    }
    if (nsamples != _dedispersion_samples)
    {
        generate_dmtrials_metadata(tf_obj.sample_interval(), nsamples - _max_delay);
    }
    std::shared_ptr<DmTrialsType> dmtrials_ptr = DmTrialsType::make_shared(_dm_trial_metadata, tf_obj.start_time());
    DmTrialsType& dmtrials = *(dmtrials_ptr);
    NumericalT const* tf_data = static_cast<NumericalT const*>(data.data());
    for (std::size_t dm_idx = 0; dm_idx < this->_algo_config.dm_trials().size(); ++dm_idx)
    {
        auto& current_trial = dmtrials[dm_idx];
        for (std::size_t samp_idx=0; samp_idx < current_trial.size(); ++samp_idx)
        {
            float sum = 0.0f;
            for (std::size_t chan_idx=0; chan_idx < nchans; ++chan_idx)
            {
                std::size_t delay = static_cast<std::size_t>(_dm_factors[chan_idx] * this->_algo_config.dm_trials()[dm_idx].value());
                std::size_t input_idx = (samp_idx + delay) * nchans + chan_idx;

                if (input_idx >= data.data_size())
                {
                    PANDA_LOG_ERROR << "Input index beyond end of buffer (" << input_idx << " >= " << data.data_size() << ")";
                }
                sum += (float) tf_data[input_idx];
            }
            current_trial[samp_idx] = sum;
        }
    }
    _dm_trials_handler(dmtrials_ptr);
}

template<typename PoolType, typename NumericalT>
void Ddtr<PoolType, NumericalT>::operator()(TimeFrequencyType const& data)
{
    if (_first_call)
    {
        DmListType dm_trials = this->_algo_config.dm_trials();
        PANDA_LOG << "Num DM trials: " << dm_trials.size();
        Dm max_dm = *std::max_element(dm_trials.begin(), dm_trials.end());
        FrequencyListType const& channel_freqs = data.channel_frequencies();
        FrequencyType freq_top = *std::max_element(channel_freqs.begin(), channel_freqs.end());
        FrequencyType freq_bottom = *std::min_element(channel_freqs.begin(), channel_freqs.end());
        _max_delay = std::size_t((this->_algo_config.dm_constant().value()
                        * (1.0/(freq_bottom*freq_bottom) -  1.0/(freq_top*freq_top))
                        * max_dm / data.sample_interval()).value()) + 1;
        for (auto freq: channel_freqs)
        {
            double factor = (this->_algo_config.dm_constant().value() * (1.0/(freq*freq) -  1.0/(freq_top*freq_top)) / data.sample_interval()).value();
            PANDA_LOG_DEBUG << "Frequency: " << freq << "  Reference: " << freq_top << "  DM constant: "
                            << this->_algo_config.dm_constant() << "  Sampling interval: " << data.sample_interval() <<  "  DM factor: " << factor;
            _dm_factors.push_back(factor);
        }

        _dedispersion_samples = this->_impl_config.dedispersion_samples();
        if (_dedispersion_samples < 2 * _max_delay)
        {
            PANDA_LOG_WARN << "Requested number of samples to dedisperse ("
                           << this->_impl_config.dedispersion_samples()
                           << ") is less than twice the max dispersion delay ("
                           << 2 * _max_delay << "): Setting number of samples to dedisperse to "
                           << 2 * _max_delay;
            _dedispersion_samples = 2 * _max_delay;
        }

        generate_dmtrials_metadata(data.sample_interval(), _dedispersion_samples - _max_delay);
        _agg_buffer_filler_ptr->resize(_dedispersion_samples * data.number_of_channels());
        _agg_buffer_filler_ptr->set_overlap(_max_delay * data.number_of_channels());
        _first_call = false;
    }
    *(_agg_buffer_filler_ptr) << data;
}

template<typename PoolType, typename NumericalT>
void Ddtr<PoolType, NumericalT>::generate_dmtrials_metadata(TimeType sample_interval, std::size_t nsamples)
{
    _dm_trial_metadata.reset(new data::DmTrialsMetadata(sample_interval, nsamples));
    for (auto dm: this->_algo_config.dm_trials())
    {
        _dm_trial_metadata->emplace_back(dm, 1);
    }
}

} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska

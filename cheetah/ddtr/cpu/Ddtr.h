/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_CPU_DDTR_H
#define SKA_CHEETAH_DDTR_CPU_DDTR_H

#include "cheetah/ddtr/cpu/Config.h"
#include "cheetah/ddtr/cpu/DdtrBase.h"
#include "cheetah/ddtr/Config.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/utils/AlgorithmBase.h"
#include "panda/AggregationBufferFiller.h"

namespace ska {
namespace cheetah {
namespace ddtr {
namespace cpu {

template<typename PoolType, typename NumericalT>
class Ddtr : public DdtrBase
{
    private:
        typedef DdtrBase BaseT;
        typedef ddtr::DdtrBase::DedispersionHandler DedispersionHandler;
        typedef ddtr::DdtrBase::DmTrialsType DmTrialsType;
        typedef ddtr::Config::Dm Dm;
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;
        typedef ska::panda::AggregationBufferFiller<TimeFrequencyType> BufferFillerType;
        typedef typename BufferFillerType::AggregationBufferType BufferType;
        typedef std::vector<ddtr::Config::Dm> DmListType;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;
        typedef std::vector<FrequencyType> FrequencyListType;

    public:
        
        Ddtr(ddtr::Config const& algo_config
            , DedispersionHandler& dm_trials_handler
            , PoolType& pool);
        Ddtr(Ddtr const&) = delete;
        Ddtr(Ddtr&&);
        ~Ddtr();

        /**
         * @brief dedispersion of time frequency data on CPU
         *  
         * @detail DmTialsHandler called when buffer is full.  
         *         Three tests written namely:
         *           1) To check if the Dedispersion Handler is called
         *           2) To check the actual dedispersion algoithm
         *           3) To check if dedispersion works for data with noise   
         *  
         * @param data   A TimeFrequency block and cpu resource 
         *
         * @return DmTime sequence i.e. timeseries for each DM trial value
         */
        void operator()(panda::PoolResource<cheetah::Cpu>& cpu
            , BufferType const& data);

        /**
         * @brief forwards data to aggregation buffer
         *
         * @detail the buffer is filled with dispersed data continously.
         *
         * @param data  inputs are the buffer data  
         */
        void operator()(TimeFrequencyType const& data);

    private:
        void generate_dmtrials_metadata(TimeType sample_interval, std::size_t nsamples);

    private:
        DedispersionHandler& _dm_trials_handler;
        PoolType& _pool;
        std::unique_ptr<BufferFillerType> _agg_buffer_filler_ptr;
        bool _first_call;
        std::size_t _max_delay;
        std::size_t _dedispersion_samples;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        std::vector<double> _dm_factors;

};


} // namespace cpu
} // namespace ddtr
} // namespace cheetah
} // namespace ska

#include "cheetah/ddtr/cpu/detail/Ddtr.cpp"

#endif // SKA_CHEETAH_DDTR_CPU_DDTR_H

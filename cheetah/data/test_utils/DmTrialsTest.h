#ifndef SKA_CHEETAH_DATA_DMTRIALSTEST_H
#define SKA_CHEETAH_DATA_DMTRIALSTEST_H

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace data {
namespace test {

template <typename Arch, typename T>
struct DmTrialsTestTraits
{
    typedef Arch Architecture;
    typedef T ValueType;
};

/**
 * @brief
 *
 * @details
 *
 */
template <typename DmTrialsTestTraitsType>
class DmTrialsTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        DmTrialsTest();
        ~DmTrialsTest();

    private:
};

template <typename TypeParam>
struct Tester
{
    static void size_test();
    static void iterator_test();
};

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska

#include "cheetah/data/test_utils/detail/DmTrialsTest.cpp"

#endif // SKA_CHEETAH_DATA_DMTRIALSTEST_H
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <type_traits>

namespace ska {
namespace cheetah {
namespace data {

template <class Arch, typename NumericalT>
TimeFrequency<Arch, NumericalT>::TimeFrequency()
    : BaseT( pss::astrotypes::DimensionSize<Frequency>(0)
           , pss::astrotypes::DimensionSize<Time>(0))
{}

template <class Arch, typename NumericalT>
TimeFrequency<Arch, NumericalT>::TimeFrequency(DimensionSize<Time> number_of_spectra, DimensionSize<Frequency> number_of_channels)
    : BaseT( pss::astrotypes::DimensionSize<Frequency>(number_of_channels)
           , pss::astrotypes::DimensionSize<Time>(number_of_spectra))
    , _flags(std::make_shared<TimeFrequencyFlagsType>(number_of_spectra, number_of_channels))
{
}

template <class Arch, typename NumericalT>
TimeFrequency<Arch, NumericalT>::TimeFrequency(DimensionSize<Frequency> number_of_channels, DimensionSize<Time> number_of_spectra)
    : BaseT( pss::astrotypes::DimensionSize<Frequency>(number_of_channels)
           , pss::astrotypes::DimensionSize<Time>(number_of_spectra))
    , _flags(std::make_shared<TimeFrequencyFlagsType>(number_of_spectra, number_of_channels))
{
}

template <class Arch, typename NumericalT>
TimeFrequency<Arch, NumericalT>::~TimeFrequency(){}

template <class Arch, typename NumericalT>
//std::size_t TimeFrequency<Arch, NumericalT>::number_of_channels() const {return this->fast_axis_length();}
std::size_t TimeFrequency<Arch, NumericalT>::number_of_channels() const {return this->template dimension<Frequency>();}

template <class Arch, typename NumericalT>
//std::size_t TimeFrequency<Arch, NumericalT>::number_of_spectra() const {return this->slow_axis_length();}
std::size_t TimeFrequency<Arch, NumericalT>::number_of_spectra() const {return this->template dimension<Time>();}

template <class Arch, typename NumericalT>
typename TimeFrequency<Arch, NumericalT>::TimePointType const& TimeFrequency<Arch, NumericalT>::start_time() const {return this->_start_time;}

template <class Arch, typename NumericalT>
typename TimeFrequency<Arch, NumericalT>::TimePointType TimeFrequency<Arch, NumericalT>::start_time(std::size_t offset) const
{
    return this->_start_time + std::chrono::duration<double>(_sample_interval.value() * offset);
}

template <class Arch, typename NumericalT>
void TimeFrequency<Arch, NumericalT>::start_time(TimePointType const& start_time) { this->_start_time = start_time; }

template <class Arch, typename NumericalT>
typename TimeFrequency<Arch, NumericalT>::TimePointType TimeFrequency<Arch, NumericalT>::end_time() const
{
    return this->_start_time + (number_of_spectra() - 1) * _sample_interval;
}

template <class Arch, typename NumericalT>
typename TimeFrequency<Arch, NumericalT>::TimeType TimeFrequency<Arch, NumericalT>::sample_interval() const {return this->_sample_interval;}

template <class Arch, typename NumericalT>
void TimeFrequency<Arch, NumericalT>::sample_interval(TimeFrequency<Arch, NumericalT>::TimeType dt) {this->_sample_interval = dt;}

template <class Arch, typename NumericalT>
std::vector<typename TimeFrequency<Arch, NumericalT>::FrequencyType> const& TimeFrequency<Arch, NumericalT>::channel_frequencies() const
{
    return this->_frequency_index;
}

template<class Arch, typename NumericalT>
std::pair<FrequencyType, FrequencyType> TimeFrequency<Arch, NumericalT>::low_high_frequencies() const
{
    if(_frequency_index.empty()) return std::pair<FrequencyType, FrequencyType>(FrequencyType(0.0 * hz) , FrequencyType(0.0 * hz) );
    if(this->_frequency_index[0] > this->_frequency_index.back()) {
        return std::make_pair(_frequency_index.back(), _frequency_index.front());
    } else {
        return std::make_pair(_frequency_index[0], _frequency_index.back());
    }
}

template<class Arch, typename NumericalT>
void TimeFrequency<Arch, NumericalT>::set_channel_frequencies_const_width(TimeFrequency<Arch, NumericalT>::FrequencyType const& start,
                                              TimeFrequency<Arch, NumericalT>::FrequencyType const& delta)
{
    _frequency_index.resize(number_of_channels());
    for(unsigned i=0U; i < number_of_channels(); ++i) {
        _frequency_index[i]= start + delta * (double)i;
    }
}

template<typename Arch, typename NumericalT, typename FreqType>
static typename
TimeFrequency<Arch, NumericalT>::FrequencyType convert_freq(FreqType const& f)
{
    return static_cast<typename TimeFrequency<Arch, NumericalT>::FrequencyType>(f); // units require explicit conversion
}

template<class Arch, typename NumericalT>
template<class InputIterator>
void TimeFrequency<Arch, NumericalT>::set_channel_frequencies(InputIterator begin, InputIterator const end){
    //this->_frequency_index.resize(end-begin);
    std::transform(begin,end,std::back_inserter(this->_frequency_index), convert_freq<Arch, NumericalT, typename std::decay<decltype(*begin)>::type> );
}

template <class Arch, typename NumericalT>
uint64_t TimeFrequency<Arch, NumericalT>::first_sample_offset() const{
    return this->_first_sample_offset;
}

template <class Arch, typename NumericalT>
void TimeFrequency<Arch, NumericalT>::first_sample_offset(uint64_t offset){
    this->_first_sample_offset = offset;
}

template<class Arch, typename NumericalT>
void TimeFrequency<Arch, NumericalT>::set_channel(unsigned channel_number, DataType const& data)
{
    //auto slice = this->slice(0, number_of_spectra() - 1, channel_number, channel_number);
    Channel channel = this->channel(channel_number);
    std::fill(channel.begin(), channel.end(), data);
}

template<class Arch, typename NumericalT>
std::shared_ptr<typename TimeFrequency<Arch, NumericalT>::TimeFrequencyFlagsType> TimeFrequency<Arch, NumericalT>::get_flags()
{
    return _flags;
}

template<class Arch, typename NumericalT>
bool TimeFrequency<Arch, NumericalT>::operator==(TimeFrequency const& o) const
{
    return static_cast<BaseT const&>(*this) == static_cast<BaseT const&>(o);
}

} // namespace data
} // namespace cheetah
} // namespace ska


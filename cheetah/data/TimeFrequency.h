/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_TIMEFREQUENCY_H
#define SKA_CHEETAH_TIMEFREQUENCY_H

#include "cheetah/data/TimeFrequencyFlags.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/chrono.h"
#include "panda/DataChunk.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include "pss/astrotypes/types/TimeFrequency.h"
#include <boost/units/systems/si/frequency.hpp>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/quantity.hpp>
#include <vector>
#include <chrono>
#pragma GCC diagnostic pop

namespace ska {
namespace cheetah {
namespace data {

using pss::astrotypes::units::Frequency;
using pss::astrotypes::units::Time;
template<typename T> using DimensionSize = pss::astrotypes::DimensionSize<T>;
template<typename T> using DimensionIndex = pss::astrotypes::DimensionIndex<T>;
template<typename T> using DimensionSpan = pss::astrotypes::DimensionSpan<T>;

/**
 * @brief A Time/Frequency data chunk.
 *
 * @details Contiguous block of time samples and frequency channels.
 *
 * The frequencies represented in each time sample shuld be set/referenced from
 * with the channel_frequencies methods.
 *
 * Each time sample is represents an indentical frequency range.
 *
 * @example
 * @code
 * #include "cheetah/data/TimeFrequecy.h"
 * #include <algorithm>
 *
 * TimeFrequecy<Cpu, uint8_t> tf_block(DimensionSize<Time>(1000), DimensionSize<Frequency>(4096));
 * ...
 * // using the iterator interface
 * for(unsigned i=0; i < tf_block.number_of_spectra(); ++i) {
 *        // iterate over channels
 *       Spectra spectrum = tf_block.spectrum(i);
 *       auto channel_it = spectrum.begin();
 *       while(channel_it != spectrum.end())
 *       {
 *          // do something with the channel value 
 *          ++channel_it;
 *       }
 *       // or use the std::algoritms e.h. fill with zeros
 *       std::fill(spectrum.begin(), spectrum.end(), 0U);
 * }
 *
 * // using the operator[] interface
 * tf_block[DimesnionIndex<Time>(2)][DimensionIndex<Frequency>(3)] = 10;
 *
 * @endcode
 * @endexample
 *
 */

template <class Arch=Cpu, typename NumericalT=uint8_t>
class TimeFrequency : public pss::astrotypes::TimeFrequency<NumericalT> // TODO Arch in base type -> use allocator
                    , public panda::DataChunk<TimeFrequency<Arch, NumericalT>>
{
        typedef pss::astrotypes::TimeFrequency<NumericalT> BaseT;

    public:
        typedef TimeFrequencyFlags<Arch> TimeFrequencyFlagsType;
        /**
         * @brief the underlying data storage type for the amplitude of the signal
         */
        typedef NumericalT DataType;
        typedef boost::units::quantity<MegaHertz, double> FrequencyType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;
        typedef cheetah::utils::ModifiedJulianClock::time_point TimePointType;
        typedef typename BaseT::iterator Iterator;
        typedef typename BaseT::const_iterator ConstIterator;

        /**
         * @brief Iterator class for accessing each time sample
         * @details note the iterator has its own begin() and end() methods
         * to generate iterators over the frequency channels of the sample
         */
        typedef typename BaseT::Spectra Spectra;
        typedef typename BaseT::ConstSpectra ConstSpectra;
        typedef typename BaseT::Channel Channel;
        typedef typename BaseT::ConstChannel ConstChannel;

        using pss::astrotypes::TimeFrequency<NumericalT>::TimeFrequency;

    public:
        /**
         * Construct an empty TimeFrequency object
         */
        TimeFrequency();

        /**
         * Construct a defined size TimeFrequency object
         */
        TimeFrequency(DimensionSize<Time> number_of_spectra, DimensionSize<Frequency> number_of_channels);
        TimeFrequency(DimensionSize<Frequency> number_of_channels, DimensionSize<Time> number_of_spectra);
        ~TimeFrequency();

        /**
         * @brief use shared_from_this() to move
         */
        //TimeFrequency(TimeFrequency&&) = delete;

        /**
         * @returns the absolute time the first time sample corresponds to.
         */
        TimePointType const& start_time() const;

        /**
         * @returns the absolute time the Nth time sample corresponds to.
         */
        TimePointType start_time(std::size_t offset) const;

        /**
         * @brief      Set the start time of the instance
         *
         * @param      start_time  The start time (MJD) of the instance
         */
        void start_time(TimePointType const& start_time);

        /**
         * @returns the absolute time the last time sample corresponds to.
         */
        TimePointType end_time() const;

        /**
         * @returns the integer number of channels in the block
         */
        std::size_t number_of_channels() const;

        /**
         * @returns the integer number of samples in the block
         */
        std::size_t number_of_spectra() const;

        /**
         * @returns the sample interval in seconds
         */
        TimeType sample_interval() const;

        /**
         * @set the sample interval in seconds
         */
        void sample_interval(TimeType dt);

        /**
         * @returns a vector of frequencies that are represented in each time sample
         */
        std::vector<FrequencyType> const& channel_frequencies() const;

        /**
         * @returns returns the lowest and highest frequencies represented in channel_frequenceis
         */
        std::pair<FrequencyType, FrequencyType> low_high_frequencies() const;

        /**
         * @brief Set the frequency index based on evenly spaced channels
         * @details
         *  Assumes that the number_of_channels has already been set
         */
        void set_channel_frequencies_const_width(FrequencyType const& start, FrequencyType const& delta);

        /**
         * Set the frequency index based on arbitary channels
         */
        template<class InputIterator>
        void set_channel_frequencies(InputIterator begin, InputIterator const end);

        /**
         * Get the absolute sample number of the first sample of this
         * TimeFrequency object. Sample number is monotonically incrementing
         * over the observation. Probably the first sample of the observation
         * is sample zero.
         */
        uint64_t first_sample_offset() const;

        /**
         * Set the absolute sample number
         */
        void first_sample_offset(uint64_t offset);

        /**
         * @brief sets the value of the specified channel to the provided
         * value across all time samples
         */
        void set_channel(unsigned channel_number, DataType const& value);

        /**
         * @brief return a shared pointer to the flags
         */
        std::shared_ptr<TimeFrequencyFlagsType> get_flags();

        /**
         * @brief return true if equivalent data
         */
        bool operator==(TimeFrequency const&) const;

    private:
        std::vector<FrequencyType> _frequency_index;
        TimeType _sample_interval;
        TimePointType _start_time;
        uint64_t _first_sample_offset=0;
        std::shared_ptr<TimeFrequencyFlagsType> _flags;
};


} // namespace data
} // namespace cheetah
} // namespace ska

namespace pss {
namespace astrotypes {

template<typename Alloc, typename T>
struct has_exact_dimensions<ska::cheetah::data::TimeFrequency<T, Alloc>, units::Time, units::Frequency> : public std::true_type
{
};

} // namespace pss
} // namespace astrotypes

#include "detail/TimeFrequency.cpp"

#endif // SKA_CHEETAH_TIMEFREQUENCY_H

#ifndef SKA_CHEETAH_DATA_TIMEFREQUENCYFLAGS_H
#define SKA_CHEETAH_DATA_TIMEFREQUENCYFLAGS_H

#include "cheetah/data/DataSequence2D.h"
#include "panda/DataChunk.h"
#include <vector>

namespace ska {
namespace cheetah {
namespace data {

typedef bool FlagsType;

template<class Arch=Cpu>
class TimeFrequencyFlags : public DataSequence2D<Arch, FlagsType>, public panda::DataChunk<TimeFrequencyFlags<Arch>>
{
    public:
        /**
         * @brief Iterator class for accessing each time sample
         * @details note the iterator has its own begin() and end() methods
         * to generate iterators over the frequency channels of the sample
         */
        typedef typename DataSequence2D<Arch, FlagsType>::PartialRange ChannelRange;
        typedef typename DataSequence2D<Arch, FlagsType>::ConstPartialRange ConstChannelRange;

    public:
        /**
         * Construct an empty TimeFrequencyFlags object
         */
        TimeFrequencyFlags();

        /**
         * Construct a defined size TimeFrequencyFlags object
         */
        TimeFrequencyFlags(std::size_t number_of_samples, std::size_t number_of_channels);
        ~TimeFrequencyFlags();

        /**
         * @returns the integer number of channels in the block
         */
        std::size_t number_of_channels() const;

        /**
         * @returns the integer number of samples in the block
         */
        std::size_t number_of_samples() const;

        /**
         * @returns an iterator that extends across all the channels in a single sample
         */
        ChannelRange time_sample(std::size_t time_sample_number);

        /**
         * @returns an iterator that extends across all the channels in a single sample
         */
        ConstChannelRange time_sample(std::size_t time_sample_number) const;

        /**
         * @brief return an iterator across time for a single channel
         */
        ChannelRange channel(unsigned int channel_number);

        /**
         * @brief reset all flags to the dspecified value
         */
        void reset(bool v = false);
};

} // namespace data
} // namespace cheetah
} // namespace ska

#include "detail/TimeFrequencyFlags.cpp"

#endif // SKA_CHEETAH_DATA_TIMEFREQUENCYFLAGS_H

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fdas/Fdas.h"
#include "cheetah/fdas/opencl/Fdas.h"
#include <iostream>


namespace ska {
namespace cheetah {
namespace fdas {


template<typename Handler>
Fdas<Handler>::Fdas(ConfigType const& c, Handler& handler)
    : _task(c.pool(), handler)
{
    if(c.active()) {
      if(c.opencl_algo_config().active()) {
	std::cout << "fdas::opencl algorithm activated" << std::endl;
	_task.template set_algorithms<fdas::opencl::Fdas>
	  ( fdas::opencl::Fdas(c.opencl_algo_config()) );
      }
      else {
	std::cout << "WARNING: no Fdas algorithm has been specified" << std::endl;
        }
    }
}

template<typename Handler>
Fdas<Handler>::~Fdas()
{
}

template<typename Handler>
void Fdas<Handler>::operator()(DataType& data)
{ 
    auto ptr = data.shared_from_this();
    _task.submit(ptr);
}

} // namespace fdas
} // namespace cheetah
} // namespace ska

#include "cheetah/dred/cuda/detail/Normaliser.cuh"

#define LN4 1.3862943611198906f

namespace ska {
namespace cheetah {
namespace dred {
namespace cuda {
namespace detail {


template <typename T>
inline __host__ __device__
thrust::complex<T> PowerNormalisingFunctor<T>::operator()(thrust::complex<T> input, T local_median) const
{
    return input * T(sqrtf(LN4/local_median));
}


} // namespace detail
} // namespace cuda
} // namespace dred
} // namespace cheetah
} // namespace ska
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DRED_CONFIG_H
#define SKA_CHEETAH_DRED_CONFIG_H

#include "cheetah/utils/Config.h"
#include "cheetah/dred/cuda/Config.h"
#include "cheetah/pwft/Config.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/DedispersionMeasure.h"
#include "panda/PoolSelector.h"

#include <vector>

namespace ska {
namespace cheetah {
namespace dred {

/**
 * @brief Algorithm configuration for the Dred module
 *
 * @details
 *
 */

class Config : public cheetah::utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief Configuration details for the cuda based RFIM algorithm
         */
        cuda::Config const& cuda_config() const;

        /**
         * @brief Configuration object for pwft module used by dred
         */
        pwft::Config const& pwft_config() const;

        /**
         * @brief      The oversmoothing factor for median window calculation
         *
         * @detail     The oversmoothing factor refers to the ratio of median
         *             smoothing window size to the smearing of the most accelerated
         *             signal we are interested in preserving in our data set.
         */
        std::size_t oversmoothing_factor() const;
        void oversmoothing_factor(std::size_t);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        cuda::Config _cuda_config;
        pwft::Config _pwft_config;
        //data::AccelerationType _maximum_acceleration; TODO: replace argument in process call by look-up from config
        std::size_t _oversmoothing_factor;
};

} // namespace dred
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DRED_CONFIG_H


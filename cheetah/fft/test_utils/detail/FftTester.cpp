/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fft/test_utils/FftTester.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "thrust/complex.h"

#include <functional>

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability>
FftTesterTraits<ArchitectureTag,ArchitectureCapability>::FftTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
fft::Fft& FftTesterTraits<ArchitectureTag,ArchitectureCapability>::api()
{
    return _api;
}

template <typename Arch, typename DeviceType, typename T>
struct FftMetadataPropagation
{
    inline static void test_r2c(DeviceType& device, fft::Fft& api)
    {
        typedef typename data::ComplexTypeTraits<Arch,T>::type ComplexT;
        typedef data::TimeSeries<Arch,T> InputType;
        typedef data::FrequencySeries<Arch,ComplexT> OutputType;
        InputType input(0.003 * data::seconds);
        OutputType output;
        input.resize(1024);
        api.process<Arch,T,InputType,OutputType>(device,input,output);
        ASSERT_EQ(output.size(),input.size()/2 + 1);
        ASSERT_EQ(output.frequency_step().value(),(1.0f/(input.sampling_interval().value() * input.size())));
    }

    inline static void test_c2r(DeviceType& device, fft::Fft& api)
    {
        typedef typename data::ComplexTypeTraits<Arch,T>::type ComplexT;
        typedef data::FrequencySeries<Arch,ComplexT> InputType;
        typedef data::TimeSeries<Arch,T> OutputType;
        InputType input(0.003 * data::hz);
        OutputType output;
        input.resize(1024);
        api.process<Arch,T,InputType,OutputType>(device,input,output);
        ASSERT_EQ(output.size(),2*(input.size() - 1));
        ASSERT_EQ(output.sampling_interval().value(),(1.0f/(input.frequency_step().value() * output.size())));
    }

    inline static void test_c2c_fwd(DeviceType& device, fft::Fft& api)
    {
        typedef typename data::ComplexTypeTraits<Arch,T>::type ComplexT;
        typedef data::TimeSeries<Arch,ComplexT> InputType;
        typedef data::FrequencySeries<Arch,ComplexT> OutputType;
        InputType input(0.066 * data::seconds);
        OutputType output;
        input.resize(1024);
        api.process<Arch,T,InputType,OutputType>(device,input,output);
        ASSERT_EQ(output.size(),input.size());
        ASSERT_EQ(output.frequency_step().value(),(1.0f/(input.sampling_interval().value() * input.size())));
    }

    inline static void test_c2c_inv(DeviceType& device, fft::Fft& api)
    {
        typedef typename data::ComplexTypeTraits<Arch,T>::type ComplexT;
        typedef data::FrequencySeries<Arch,ComplexT> InputType;
        typedef data::TimeSeries<Arch,ComplexT> OutputType;
        InputType input(0.0035 * data::hz);
        OutputType output;
        input.resize(1024);
        api.process<Arch,T,InputType,OutputType>(device,input,output);
        ASSERT_EQ(output.size(),input.size());
        ASSERT_EQ(output.sampling_interval().value(),(1.0f/(input.frequency_step().value() * output.size())));
    }
};

template <typename TestTraits>
FftTester<TestTraits>::FftTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
FftTester<TestTraits>::~FftTester()
{
}

template<typename TestTraits>
void FftTester<TestTraits>::SetUp()
{

}

template<typename TestTraits>
void FftTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(FftTester, fft_metadata_propagation)
{
    TypeParam traits;
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, float>::test_r2c(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, float>::test_c2r(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, float>::test_c2c_fwd(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, float>::test_c2c_inv(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, double>::test_r2c(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, double>::test_c2r(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, double>::test_c2c_fwd(device,traits.api());
    FftMetadataPropagation<typename TypeParam::Arch, typename TypeParam::DeviceType, double>::test_c2c_inv(device,traits.api());
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(FftTester, fft_metadata_propagation);

} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FFT_TEST_FFTTESTER_H
#define SKA_CHEETAH_FFT_TEST_FFTTESTER_H

#include "cheetah/fft/Config.h"
#include "cheetah/fft/Fft.h"
#include "cheetah/utils/test_utils/AlgorithmTester.h"
#include "cheetah/data/Units.h"

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

/**
 * @brief
 * Generic functional test for the FftTester algorithm
 *
 * @details
 * To use you will first need to create suitable Traits descriptions of the algorithm and the
 * hardware requiremnst it needs to run.
 *
 * e.g.
 * @code
 * struct CudaTraits : public FftTesterTraits<cuda::Fft::Architecture,cuda::Fft::ArchitectureCapability>
 * {
 *   typedef test::FftTesterTraits<cuda::Fft::Architecture, typename cuda::Fft::ArchitectureCapability> BaseT;
 *   typedef Fft::Architecture Arch;
 *   typedef typename BaseT::DeviceType DeviceType;
 * };
 * @endcode
 *
 * Instantiate your algorithm tests by constructing suitable @class AlgorithmTestTraits classes
 * and then instantiate them with the INSTANTIATE_TYPED_TEST_CASE_P macro
 *
 *  e.g.
 * @code
 * typedef ::testing::Types<CudaTraits, OpenClTraits> MyTypes;
 * INSTANTIATE_TYPED_TEST_CASE_P(MyAlgo, FftTester, MyTypes);
 * @endcode
 *  n.b. the INSTANTIATE_TYPED_TEST_CASE_P must be in the same namespace as this class
 *
 */

template<typename ArchitectureTag, typename ArchitectureCapability>
struct FftTesterTraits
    : public utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability>
{
    public:
        typedef utils::test::AlgorithmTesterTraits<ArchitectureTag, ArchitectureCapability> BaseT;
        typedef ArchitectureTag Arch;
        typedef typename BaseT::DeviceType DeviceType;

    public:
        FftTesterTraits();
        fft::Fft& api();

    private:
        fft::Config _config;
        fft::Fft _api;

};


template <typename TestTraits>
class FftTester : public cheetah::utils::test::AlgorithmTester<TestTraits>
{
    protected:
        void SetUp();
        void TearDown();

    public:
        FftTester();
        ~FftTester();

    private:
};

TYPED_TEST_CASE_P(FftTester);

} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/test_utils/detail/FftTester.cpp"


#endif // SKA_CHEETAH_FFT_TEST_FFTTESTER_H

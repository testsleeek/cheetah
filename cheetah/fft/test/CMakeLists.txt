include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(
    gtest_fft_src_cpu
    src/gtest_fft.cpp
)

set(
    gtest_fft_src_cuda
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_fft ${gtest_fft_src_cuda} ${gtest_fft_src_cpu})
else(ENABLE_CUDA)
    add_executable(gtest_fft ${gtest_fft_src_cpu})
endif(ENABLE_CUDA)

target_link_libraries(gtest_fft ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_fft gtest_fft)

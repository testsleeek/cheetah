#include "cheetah/fft/cuda/Fft.cuh"
#include "cheetah/fft/test_utils/FftTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace fft {
namespace cuda {
namespace test {

struct CudaTraits
    : public fft::test::FftTesterTraits<fft::cuda::Fft::Architecture,fft::cuda::Fft::ArchitectureCapability>
{
    typedef fft::test::FftTesterTraits<fft::cuda::Fft::Architecture, typename fft::cuda::Fft::ArchitectureCapability> BaseT;
    typedef Fft::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace fft
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace fft {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, FftTester, CudaTraitsTypes);

} // namespace test
} // namespace fft
} // namespace cheetah
} // namespace ska
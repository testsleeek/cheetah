/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FFT_FFT_H
#define SKA_CHEETAH_FFT_FFT_H

#include "cheetah/fft/FftType.h"
#include "cheetah/fft/Config.h"

#include "cheetah/fft/cuda/Fft.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/Architectures.h"

#include "panda/AlgorithmTuple.h"


namespace ska {
namespace cheetah {
namespace fft {

/**
 * @brief      A class for performing FFTs
 *
 * @details    The Fft class acts as a forwarding class for architecture
 *             dependent FFT implementations. Forwarding is determined by
 *             the types of the data passed to the process() method. These
 *             types also implicitly specify the type of FFT to be performed.
 */
class Fft
{
    private:
        typedef panda::AlgorithmTuple<cuda::Fft> Implementations;

    public:

        /**
         * @brief      Construct and Fft instance
         *
         * @param[in]  config    A configuration object for the Fft instance
         */
        Fft(Config const& config);

        //Copy constructors removed.
        Fft(Fft const&) = delete;

        Fft(Fft&&) = default;

        /**
         * @brief      Perform an FFT
         *
         * @detail     The type of FFT performed is deduced from the types of the inputs such that:
         *             Input                  Output                FftType
         *             TimeSeries (Real)      FrequencySeries       R2C
         *             FrequencySeries        TimeSeries (Real)     C2R
         *             TimeSeries (Complex)   FrequencySeries       C2C Forward
         *             FrequencySeries        TimeSeries (Complex)  C2C Inverse
         *
         *             This acts as a forwarding method, forwarding the relevant parameters to
         *             implementations that are determined by the resource type and the input and
         *             output types.
         *
         * @param[in]  resource   A panda::PoolResource instance specifying the resource to process on
         * @param[in]  input      A TimeSeries or FrequencySeries instance
         * @param[out] output     A TimeSeries or FrequencySeries instance
         *
         * @tparam     Arch        The cheetah architecture type
         * @tparam     BaseT       The base value type of the input and outputs, if the input is complex,
         *                         this refers to the base value type of the complex type
         * @tparam     InputType   The type of the input parameter
         * @tparam     OutputType  The type of the output parameter
         * @tparam     Args        The types of any additional parameters to be forwarded to the implementation
         */
        template <typename Arch, typename BaseT, typename InputType, typename OutputType, typename... Args>
        void process(panda::PoolResource<Arch>& resource,
            InputType const& input,
            OutputType& output,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};

} // namespace fft
} // namespace cheetah
} // namespace ska

#include "cheetah/fft/detail/Fft.cpp"

#endif // SKA_CHEETAH_FFT_FFT_H

/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_GENERATORS_RFISCENARIO_H
#define SKA_CHEETAH_GENERATORS_RFISCENARIO_H


#include "RfiGenerator.h"

namespace ska {
namespace cheetah {
namespace generators {

/**
 * @brief
 *    Collection of RFI scenarios.
 * @details
 *    Each one assigned to a a different number 
 *    in the tempalte parameter list.
 */

template<int, typename T>
class RfiScenario
{
    public:
        RfiScenario();
        virtual ~RfiScenario() = 0;

    private:
};

/**
 * @brief no rfi
 */
template<typename T>
class RfiScenario<0, T> : public RfiGenerator<T> {
        typedef RfiGenerator<T> BaseT;
        typedef typename BaseT::DataType DataType;

    public:
        RfiScenario();
        ~RfiScenario();
        void next(DataType& data) override; 
};

/**
 * @brief ramp decreasing in intensity with frequency, constant across all time scales
 */
template<typename T>
class RfiScenario<1, T> : public RfiGenerator<T> {
        typedef RfiGenerator<T> BaseT;
        typedef typename BaseT::DataType DataType;
    public:
        RfiScenario();
        ~RfiScenario();
        void next(DataType& data) override; 
};

/**
 * @brief peak of RFI in middle of block, with Gaussian wings.
 */
template<typename T>
class RfiScenario<2, T> : public RfiGenerator<T> {
        typedef RfiGenerator<T> BaseT;
        typedef typename BaseT::DataType DataType;
    public:
        RfiScenario();
        ~RfiScenario();
        void next(DataType& data) override; 
};

/**
 * @brief peaks of RFI in middle of four separate blocks, with Gaussian wings.
 */
template<typename T>
class RfiScenario<3, T> : public RfiGenerator<T> {
        typedef RfiGenerator<T> BaseT;
        typedef typename BaseT::DataType DataType;
    public:
        RfiScenario();
        ~RfiScenario();
        void next(DataType& data) override; 
};

/**
 * @brief peaks of RFI in middle of four overlapping blocks, with Gaussian wings.
 */
template<typename T>
class RfiScenario<4, T> : public RfiGenerator<T> {
        typedef RfiGenerator<T> BaseT;
        typedef typename BaseT::DataType DataType;
    public:
        RfiScenario();
        ~RfiScenario();
        void next(DataType& data) override; 
};

} // namespace generators
} // namespace cheetah
} // namespace ska
#include "detail/RfiScenario.cpp"
 
#endif // SKA_CHEETAH_GENERATORS_RFISCENARIO_H

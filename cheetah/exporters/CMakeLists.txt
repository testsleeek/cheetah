set(module_exporters_lib_src_cpu
    src/DataExportConfig.cpp
    src/DataExportStreamConfig.cpp
    src/ExporterType.cpp
    src/FileStreamerConfig.cpp
    src/OcldFileStreamer.cpp
    src/OcldFileStreamerConfig.cpp
    src/OcldFileStreamerTraits.cpp
    src/SclFileStreamer.cpp
    src/SclFileStreamerConfig.cpp
    src/SclFileStreamerTraits.cpp	
    src/SpCclFileStreamerConfig.cpp
    src/SpCclSigProcConfig.cpp
    PARENT_SCOPE
)

TEST_UTILS()

add_subdirectory(test)

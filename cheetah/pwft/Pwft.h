/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PWFT_H
#define SKA_CHEETAH_PWFT_H

#include "cheetah/pwft/Config.h"
#include "cheetah/pwft/cuda/Pwft.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/ComplexTypeTraits.h"

#include "panda/AlgorithmTuple.h"

namespace ska {
namespace cheetah {
namespace pwft {

/**
 * @brief
 * Power Spectrum Fourier Transform version / Transform / Module
 *
 * @details
 */

class Pwft
{
    private:
        typedef panda::AlgorithmTuple<cuda::Pwft> Implementations;

    public:

        Pwft(Config const& config);
        Pwft(Pwft const&) = delete;
        Pwft(Pwft&&) = default;
        ~Pwft();

        /**
         * @brief      Form power spectrum using absolute squared.
         *
         * @detail     Each power measurement is found by
         *             output[i] = |input[i]|^2
         *
         * @param      resource   A panda PoolResource object
         * @param[in]  input      A FrequencySeries object of complex data type
         * @param[out] output     A FrequencySeries object of real data type
         * @param      args...    Additional arguments to be forwarded to implementation
         *
         * @tparam     Arch       A processing architecture
         * @tparam     T          The value type of the input and output FrequencySeries
         * @tparam     Alloc      The allocator type of the input and output FrequencySeries
         * @tparam     Args       Additional template parameters for call forwarding
         */
        template <typename Arch, typename T, typename InputAlloc, typename OutputAlloc, typename... Args>
        void process_direct(panda::PoolResource<Arch>& resource,
            data::FrequencySeries<Arch,typename data::ComplexTypeTraits<Arch,T>::type,InputAlloc>const& input,
            data::PowerSeries<Arch,T,OutputAlloc>& output,
            Args&&... args);

        /**
         * @brief      Form power spectrum using absolute squared with nearest neighbour comparison.
         *
         * @detail     Each power measurement is found by comparing neighbouring bins such that
         *             output[i] = max(|input[i]|^2, |input[i]-input[i-1]|^2)
         *
         * @param      resource   A panda PoolResource object
         * @param[in]  input      A FrequencySeries object of complex data type
         * @param[out] output     A FrequencySeries object of real data type
         * @param      args...    Additional arguments to be forwarded to implementation
         *
         * @tparam     Arch       A processing architecture
         * @tparam     T          The value type of the input and output FrequencySeries
         * @tparam     Alloc      The allocator type of the input and output FrequencySeries
         * @tparam     Args       Additional template parameters for call forwarding
         */
        template <typename Arch, typename T, typename InputAlloc, typename OutputAlloc, typename... Args>
        void process_nn(panda::PoolResource<Arch>& resource,
            data::FrequencySeries<Arch,typename data::ComplexTypeTraits<Arch,T>::type,InputAlloc>const& input,
            data::PowerSeries<Arch,T,OutputAlloc>& output,
            Args&&... args);

    private:
        Config const& _config;
        Implementations _implementations;
};



} // namespace pwft
} // namespace cheetah
} // namespace ska

#include "cheetah/pwft/detail/Pwft.cpp"

#endif // SKA_CHEETAH_PWFT_H

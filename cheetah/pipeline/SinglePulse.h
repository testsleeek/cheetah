/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_SINGLEPULSE_H
#define SKA_CHEETAH_PIPELINE_SINGLEPULSE_H

#include "cheetah/pipeline/RfiDetectionPipeline.h"
#include "cheetah/sps/Sps.h"
#include "cheetah/spsift/SpSift.h"

namespace ska {
namespace cheetah {
namespace pipeline {
template<typename NumericalT>
class CheetahConfig;
class BeamConfig;

/**
 * @brief A SINGLE PULSE SEARCH PIPELINE
 * @details
 */

template<typename NumericalT>
class SinglePulse : public RfiDetectionPipeline<NumericalT>
{
        typedef RfiDetectionPipeline<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;
        typedef sps::Sps<sps::ConfigType<typename CheetahConfig<NumericalT>::PoolManagerType>> Sps;


    public:
        SinglePulse(CheetahConfig<NumericalT> const& config, BeamConfig const& beam_config);
        SinglePulse(SinglePulse&&);
        ~SinglePulse();

        void operator()(TimeFrequencyType&) override;


    protected:
        typedef typename Sps::DmTrialType DmTrialType;
        typedef typename Sps::SpType SpType;

        class DedispersionHandler {
            public:
                DedispersionHandler(SinglePulse&);
                void operator()(std::shared_ptr<DmTrialType> const&) const;

            private:
                SinglePulse& _pipeline;
        };

        class SpsHandler {
            public:
                SpsHandler(SinglePulse&);
                void operator()(std::shared_ptr<SpType> const&) const;

            private:
                SinglePulse& _pipeline;
        };

    private:
        DedispersionHandler _dedispersion_handler;
        SpsHandler _sps_handler;
        Sps _sps;
        spsift::SpSift _spsifter;
};


} // namespace pipeline
} // namespace cheetah
} // namespace ska
#include "detail/SinglePulse.cpp"

#endif // SKA_CHEETAH_PIPELINE_SINGLEPULSE_H

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/Config.h"

namespace ska {
namespace cheetah {
namespace rfim {


Config::Config()
    : cheetah::utils::Config("rfim")
{
    add(_ampp_config);
    add(_cuda_config);
    add(_sum_threshold_config);
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& /*add_options*/)
{
/*
    add_options
    ("num_gpu", boost::program_options::value<unsigned>()->default_value(0U)->notifier( [this](unsigned n_gpu) {
                                                                                                utils::system().provision<panda::nvidia::Cuda>(_pool, n_gpu); 
                                                                                            }), "number of gpus to assign to the rfim module");
*/
}

ampp::Config const& Config::ampp_algo_config() const
{
    return _ampp_config;
}

cuda::Config const& Config::cuda_algo_config() const
{
    return _cuda_config;
}

sum_threshold::Config const& Config::sum_threshold_algo_config() const
{
    return _sum_threshold_config;
}


} // namespace rfim
} // namespace cheetah
} // namespace ska

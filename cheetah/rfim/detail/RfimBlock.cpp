#include "cheetah/rfim/RfimBlock.h"


namespace ska {
namespace cheetah {
namespace rfim {


template<class RfimDetector, class RfimPolicy>
RfimBlock<RfimDetector, RfimPolicy>::RfimBlock(RfimDetector&& detector)
    : _detector(std::move(detector))
{
}

template<class RfimDetector, class RfimPolicy>
RfimBlock<RfimDetector, RfimPolicy>::~RfimBlock()
{
}

template<class RfimDetector, class RfimPolicy>
template<typename ResourceType, typename DataType>
DataType& RfimBlock<RfimDetector, RfimPolicy>::operator()(ResourceType&& device, std::shared_ptr<DataType> data)
{
    return _policy(_detector(std::forward<ResourceType>(device), data), *data);
}

} // namespace rfim
} // namespace cheetah
} // namespace ska

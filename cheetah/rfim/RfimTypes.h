#ifndef SKA_CHEETAH_RFIM_RFIMTYPES_H
#define SKA_CHEETAH_RFIM_RFIMTYPES_H

#include "cheetah/data/TimeFrequency.h"

namespace ska {
namespace cheetah {
namespace rfim {

/**
 * @brief
 *    Common type definitions for the Rfim modules
 * @details
 *
 */
typedef data::TimeFrequency<Cpu> TimeFrequencyType;

} // namespace rfim
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_RFIM_RFIMTYPES_H

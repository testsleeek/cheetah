/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_RFIM_CUDA_RFIM_H
#define SKA_CHEETAH_RFIM_CUDA_RFIM_H

#include "cheetah/rfim/cuda/Config.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/TimeFrequency.h"
#include "panda/arch/nvidia/DeviceCapability.h"

namespace ska {
namespace cheetah {
namespace rfim {
namespace cuda {

/**
 * @brief
 *    CUDA based Radio Frequency Mitigation Algorithm
 *
 * @details
 * 
 */

class Rfim
{
    public:
        typedef cheetah::Cuda Architecture;
        typedef panda::nvidia::DeviceCapability<2,0, panda::nvidia::giga/2> ArchitectureCapabilty; // minimum device requirements

    public:
        Rfim(Config const&);
        ~Rfim();

        /**
         * @brief apply rfim mitigation to the data set
         */
        template<typename DataType>
        DataType& operator()(panda::PoolResource<Cuda>& device, std::shared_ptr<DataType> data);

    private:
};


} // namespace cuda
} // namespace rfim
} // namespace cheetah
} // namespace ska
#include "detail/Rfim.cpp"

#endif // SKA_CHEETAH_RFIM_CUDA_RFIM_H

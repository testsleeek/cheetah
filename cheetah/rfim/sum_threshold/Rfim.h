#ifndef SKA_CHEETAH_SUM_THRESHOLD_RFIM_H
#define SKA_CHEETAH_SUM_THRESHOLD_RFIM_H

#include "cheetah/rfim/sum_threshold/Config.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/rfim/RfimTypes.h"
#include "cheetah/rfim/FlaggedData.h"

namespace ska {
namespace cheetah {
namespace rfim {
namespace sum_threshold {

/**
 * @brief
 *    A CPU implementation of the sum_threshold algorithm
 *
 * @details
 * 
 */

class Rfim
{
    public:
        typedef cheetah::Cpu Architecture;

    public:
        Rfim(Config const& config);
        ~Rfim();

        /**
         * @brief task interface warpper around run
         */
        template<typename DataType=data::TimeFrequency<Cpu>>
        FlaggedData operator()(panda::PoolResource<Cpu>& thread, std::shared_ptr<DataType> data);
        //cheetah::data::TimeFrequency<Cpu>& operator()(panda::PoolResource<Cpu>& thread, std::shared_ptr<cheetah::data::TimeFrequency<Cpu>> data);

    private:
        Config const& _config;
};


} // namespace sum_threshold
} // namespace rfim
} // namespace cheetah
} // namespace ska
#include "detail/Rfim.cpp"

#endif // SKA_CHEETAH_SUM_THRESHOLD_RFIM_H 

#ifndef SKA_CHEETAH_RFIM_FLAGGEDDATA_H
#define SKA_CHEETAH_RFIM_FLAGGEDDATA_H


namespace ska {
namespace cheetah {
namespace rfim {

/**
 * @brief
 *
 * @details
 *
 */

class FlaggedData
{
    public:
        FlaggedData();

        ~FlaggedData();

    private:
};


} // namespace rfim
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_RFIM_FLAGGEDDATA_H

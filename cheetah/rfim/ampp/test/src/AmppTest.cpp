/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/ampp/test/AmppTest.h"
#include "cheetah/rfim/ampp/Rfim.h"
#include "cheetah/rfim/test_utils/RfimTester.h"
#include <memory>


namespace ska {
namespace cheetah {
namespace rfim {
namespace test {

struct AmppTraits : public RfimTesterTraits<rfim::ampp::Rfim::Architecture> {
    private:
        typedef RfimTesterTraits<rfim::ampp::Rfim::Architecture> BaseT;

    public:
        AmppTraits();

        static ResultType& apply_algorithm(DeviceType&, DataType& data);

        // return the api for the ampp algorithm
        static rfim::ampp::Rfim& algo();

        // return the configuration for this api
        static rfim::ampp::Config& config();

};

AmppTraits::AmppTraits()
{
    // initialise the object
}


rfim::ampp::Config& AmppTraits::config()
{
    static rfim::ampp::Config config;
    auto& bp_config=config.bandpass();
    bp_config.number_of_channels(30);
    boost::units::quantity<boost::units::si::frequency, double> start( 100.0 * boost::units::si::mega * boost::units::si::hertz );
    boost::units::quantity<boost::units::si::frequency, double> delta( 1.0 * boost::units::si::mega * boost::units::si::hertz );
    bp_config.start_frequency(start);
    bp_config.channel_width(delta);
    bp_config.polynomial_coefficients( { 0.2, 0.4, 0.9} );
    bp_config.rms(12090);

    return config;
}

rfim::ampp::Rfim& AmppTraits::algo()
{ 
    static rfim::ampp::Rfim algo_api(config());
    return algo_api;
}

AmppTraits::ResultType& AmppTraits::apply_algorithm(DeviceType&, DataType& data)
{
    algo().run(data);
    return data;
}

AmppTest::AmppTest()
    : ::testing::Test()
{
}

AmppTest::~AmppTest()
{
}

void AmppTest::SetUp()
{
}

void AmppTest::TearDown()
{
}

typedef ::testing::Types<AmppTraits> AmppTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Ampp, RfimTester, AmppTraitsTypes);

} // namespace test
} // namespace rfim
} // namespace cheetah
} // namespace ska

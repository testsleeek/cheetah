/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rfim/ampp/Rfim.h"
#include "cheetah/utils/BinMap.h"
#include "cheetah/data/Units.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <cmath>
#include <numeric>
#include <type_traits>

namespace ska {
namespace cheetah {
namespace rfim {
namespace ampp {


Rfim::Rfim(Config const& c)
    : _config(c)
    , _zero_dm(c.zero_dm()?0:1)
    , _bad_spectra(0)
    , _sample_counter(0)
    , _max_history(c.max_history())
{
    assert(_max_history != 0);
    _mean_buffer.set_capacity(_max_history);
    _rms_buffer.set_capacity(_max_history);
    set_bandpass(BandPass(c.bandpass()));
    BOOST_ASSERT_MSG(_bandpass.is_valid(), "Invalid BandPass");
}

Rfim::~Rfim()
{
}

Rfim::Rfim(Rfim&& c)
    : _config(c._config)
    , _bandpass(std::move(c._bandpass))
    , _zero_dm(c._zero_dm)
    , _bad_spectra(c._bad_spectra)
    , _mean_buffer(std::move(c._mean_buffer))
    , _rms_buffer(std::move(c._rms_buffer))
    , _sample_counter(c._sample_counter)
    , _max_history(c._max_history)
{
}

void Rfim::set_bandpass(BandPass bandpass)
{
    if(!bandpass.is_valid()) {
        throw ska::panda::Error("ampp::Rfim::set_bandpass() : attempt to set invalid bandpass");
    }

    _bandpass = std::move(bandpass);
    /*
   // fill in the history as if it has a perfect bandpass
    for(std::size_t i = 0U; i < _history.size(); ++i) {
        _history[i] = 0.0f;
        _history_mean[i] = _band_pass.mean();
        _history_rms[i] = _band_pass.rms();
        _history_good_channel_sum[i] = i;
    }
    */
}
  /*
void Rfim::set_max_history(unsigned length)
{
    // Set the capacity of the circular buffers of mean and rms values
    // to the correct size
    _mean_buffer.set_capacity(length);
    _rms_buffer.set_capacity(length);
}
  */

} // namespace ampp
} // namespace rfim
} // namespace cheetah
} // namespace ska


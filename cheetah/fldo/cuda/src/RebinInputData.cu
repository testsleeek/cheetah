
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/detail/FldoUtils.h"
#include <vector>


namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

/*
 *
 * void rebin_input_data(int start, int current, int nchannels, std::vector<CandidateRebin> const &rebin)
 *
 * @brief Sum-up adiacent time samples depending on the rebin value.
 *
 * @param start         the starting rebinning index
 * @param current       the current rebinning index
 * @param nchannels     the number of freq. channels
 * @param rebin         the vector with the rebinning structures
 *
 * @return On failure throws a runtime_error exception.
 */
void rebin_input_data(int start, int current, int nchannels, std::vector<CandidateRebin> const &rebin)
{
    //dim3 threadsPerBlock;       // number of kernel threads per block
    //dim3 blocksPerGrid;         // number of kernel blocks for grid

    int tile_dimx = 32;         // tile dimension in x = num of threads in x
    int tile_dimy = 32;         // tile dimension in y = num of threads in y
    int nblock_y = (nchannels + (tile_dimy - 1))/tile_dimy;
    int nblock_x  = (rebin[start].pitch_dim + (tile_dimx - 1))/tile_dimx;
    dim3 grid(nblock_x, nblock_y);
    dim3 threads(tile_dimx, tile_dimy);
    PANDA_LOG_DEBUG << "Call to bin input data: threads = ("
                    << tile_dimx
                    << ", "
                    << tile_dimy
                    << ") blocks = ("
                    << nblock_x
                    << " , "
                    << nblock_y
                    << ")";
    PANDA_LOG_DEBUG << "rebin_input_data: start binning: "
                    << start
                    << "("
                    << rebin[start].rebin
                    << ") current binning: "
                    << current
                    << "("
                    << rebin[current].rebin
                    << ")";
    //get the ratio between the first and current prebin value
    int prebin = rebin[current].rebin/rebin[start].rebin;
    // we handle the prebinning. (we hope it not end inn a mess!)

    // binInputKernel is launched in a stream different from the default one.
    // The folding on the candidates belonging to the current prebin list,
    // has to wait for the end of this kernel. In corrispondence of this
    // kernel it is registered and event to signal its ending.
#ifdef TRANSPOSE_SHFL
    binInputKernel_shfl<<<grid,threads, 0, rebin[current].stream>>>
    (rebin[current].d_out,
     rebin[start].d_out,
     rebin[start].pitch_dim,
     rebin[current].pitch_dim,
     prebin);
#else
    int shared_memory_size = tile_dimx * tile_dimy * sizeof(float);
    binInputKernel<<<grid,threads, shared_memory_size, rebin[current].stream>>>
    (rebin[current].d_out,
     rebin[start].d_out,
     nchannels,
     rebin[start].pitch_dim,
     rebin[current].pitch_dim,
     prebin);
#endif

    CUDA_ERROR_CHECK(cudaGetLastError());
    CUDA_ERROR_CHECK(cudaEventRecord(rebin[current].event, rebin[current].stream));
    PANDA_LOG_DEBUG << "Succes in rebinning kernel execution";
}
} // utils
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

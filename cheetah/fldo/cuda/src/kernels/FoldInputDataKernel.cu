/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * @brief
 *  GPU main kernels
 *
 * @details
 *
 * initial assumptions:
 * Data are read in into main CPU memory. They are divided into 64
 * subintegrations (blocks). At least one full block is present at the same
 * time on main memory. Data are in unsigned char format (8 bit)
 * Block dimension is (4096 frequency channels) X (up to 187500 sequential
 * measures).  This lead to up to 3GB. Too much, we will split either in
 * frequency or in time.
 *
 * Each thread reads and process a single frequency num_frequency =
 * blockIdx.x and inside GPU frequency number is threadIdx.x data are read
 * and then converted into streams of float (4bytes) in GPU main memory.
 *
 *
 */

// includes, system

#include <float.h>

namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
//
// void folding_worker(float *g_in, float *folded, float *weight, int local_measures,
//                                          int candidate, int xfactor, int warp_count,
//                                          double tobs,
//                                          int max_phases, int isubint,
//                                          int threadblock_memory)
//
// g_in                 input converted data
// folded               array to store folded data
// weight               array to store weight data
// pitch_dim            the data array row length (padded with 0)
// local_measure        number of sample per channel and per subint ( <=  pitch_dim)
// candidate            the candidate number
// xfactor              the period multiplicative factor
// warp_count           the number of warp programmed on each SM
// tobs                 the observation time (to set as constant in future ??)
// max_phases           the maximum number of phase bins for the current candidate list
// isubint              the current subintegration number
// threadblock_memory   number of shared memory elements.
//
//
// Arrangement of the data into folded and weight arrays for the first
// candidate. The same organization is replicated for each other candidate
// [0, 127]: phase bins range
// ---------------------------------------------------------------------------------------
// |0|1|...|127 |0|1|...|127|...|0|1|...|127|0|1|...|127|0|1|...|127|...|0|1|...|127|....
// ---------------------------------------------------------------------------------------
// |<-band 0  ->|<-band 1 ->|...|<-band N ->|<-band 0 ->|<-band 1 ->|...|<-band N ->|
// <------------- subint 0  --------------->|<----------- subint 1  ----------------> ... <-- subint N  ->
// <------------                     candidate 0                                                 -------->
//
//
__global__ void folding_worker(float *g_in, float *folded, float *weight,
                               int pitch_dim, int local_measures, int candidate, int xfactor,
                               int rebin, int shared_phases, int warp_count,
                               double tobs, int max_phases, int isubint, int threadblock_memory)
{

    /* threadIdx.x:      the current sample inside thread block
     * threadIdx.y       the current channel inside thread block
     * blockDim.x        the stride in samples
     * blockIdx.x:       subband index
     * blockDim.y        the stride in channels
     * gridDim.x:        number of subbands
     */
    int subband_idx  = blockIdx.x;      // the sub-band index (0, 1, ...64)
    int chan_idx = threadIdx.y;         // index of channel in current subband
                                        // (0, 1, .., n_chan_band)

    int time_idx = 0;                   // index to subint measures
    // start of phase array for current operations
    int start_phase = max_phases * subband_idx + isubint * max_phases * gridDim.x +
                      candidate * d_nsubints * max_phases *gridDim.x;
    int channel = 0;    // index of local channel
    // To be computed into CPU and put in a global tabellar form  (DONE on 24/12/2013)
    int nbins   = d_nbins[candidate];    // total number of phases of the candidate
    // physical values.
    // The extra delay in for this channel based on the DM
    // equation (see Lorimer & Kramer book)
    // time of local sample
    double time_s = 0.; // <--- WARNING!!!!!!!!!!!!
    // float has only 7 digit mantissa. for a full 600 sec datafile this is not enough!
    double phase     = 0.; // phase of single measure

    int bin1 = 0;       // profile index of the first phase bin
    int bin2 = 0;       // profile index of the second phase bin
    int start_data = subband_idx * d_nchan_per_band * pitch_dim;
    int start_idx = 0;          //index of the first time sample of each subband
    float input_temp = 0;       //the input sample value
    float *ptr = 0;             //pointer to the data array
    float frac_floor = 0.;
    float frac = 0;
    float bin2_frac = 0.;       //the second phase bin weight
    float val2 = 0.;            //the second phase bin intensity value
    extern   __shared__ float hist_profile[];   //dinamically allocated shared memory
    int idx = threadIdx.x + blockDim.x * threadIdx.y;   //thread global id
    // (idx >> 5) gives the warp number
    // 1 warp = 32 threads
    //
    // Att: if for the future GPUs 1 warp != 32, next lines has to be changed!!!
    // The warp_size is obtained by the cudaDeviceProp structure.
    float *warp_profile = hist_profile + (idx >> 5) * shared_phases;
    float *hist_weight = hist_profile + warp_count * shared_phases;
    float *warp_weight=  hist_weight + (idx >> 5) * shared_phases;

    // zero the shared memory
    for (int i = idx; i < threadblock_memory ; i += blockDim.x * blockDim.y) {
        hist_profile[i] = 0.;
        hist_weight[i] = 0.;
    }
    __syncthreads();

    // loop on time measures  (local_measures at time, nsamp/fold.nsubints)
    int nchan =  d_nchan_per_band * subband_idx; //number of the first channel of the subband
    int nsamp = local_measures * isubint;       //number of the first time sample of the
                                                //sub-integration
    float nudot_contrib = 0.;
    double t0 = -tobs * 0.5;
    for (time_idx = threadIdx.x ; time_idx < local_measures; time_idx += blockDim.x) {
        start_idx = start_data + time_idx;                  // sample position into the data array
        time_s = t0 + d_tsamp * rebin *(nsamp + time_idx);   // sample position in time
        ptr = g_in + start_idx;
        nudot_contrib = time_s * time_s * d_nudot[candidate] * 0.5;//phase shift due to acceleration
        for (chan_idx = threadIdx.y ; chan_idx < d_nchan_per_band; chan_idx += blockDim.y) {
            channel = chan_idx + nchan;                     // index of local channel
            //input_temp = __ldg(&ptr[chan_idx * pitch_dim]);
            input_temp = ptr[chan_idx * pitch_dim];
            // we compute phase
            phase = (time_s + d_dm[candidate] * d_delta_freq[channel]) *d_nu[candidate] +
                                            nudot_contrib;
            frac = (float) ((phase - floor(phase)) * nbins);
            bin2_frac = modff(frac, &frac_floor);

            //bin1 = ((int)frac_floor) % nbins;
            //bin1 = ((int)frac_floor);

            // ATT:
            // bin1 can be less or equal to nbins (nbins is the max number
            // of phase bins for the specific candidate), so the mod
            // operation is not necessary.
            // To avoid mod operation also in the calculation of bin2
            // (to limit the number of internal registers used) it may
            // happen that the data are stored also in the element  (nbins + 1) of
            // the profile array.
            // In the computation of the final profile we will add the
            // value of element nbins to that stored in  element 0,
            // and the one of the element (nbins + 1) in element 1.
            //
            //bin2 = ((int)ceilf(frac)) % nbins;
            //bin2 = ((int)ceilf(frac));
            //
            // if at the end we sum up to the last  phase
            // (nbins) to the first one (0)
            // OSS: here frac > 0 so the integer part is equal to floor(frac)
            bin1 = (int)frac_floor;
            val2 =  bin2_frac * input_temp;
            bin2 = (bin1 + 1);
            // input Index is computed as follows:
            // time_idx is the index of the measures (along time)
            //          of the current subint of the current channel
            // chan_idx * local_measures is the offset of start of
            //          measure of current channel from the beginning of
            //          channel block
            // The following changes only in the outer loop:
            // n_chan_band * blockIdx.x * local_measures is the offset
            //          the start of measure of current channel block
            // input_temp = g_in[time_idx + chan_idx * local_measures +
            //             n_chan_band * blockIdx.x * local_measures];

            // output
            // folded is structured as a (multidim) array:

            // max_phases  number of phases
            // weight is exactly the same.
            // start_phase = candidate * blockDim.x + subband_idx * gridDim.x

            //read and update data use atomic functions because there can be
            //collisions between threads accessing the same address
            atomicAdd((float *)&warp_profile[bin1], input_temp - val2);
            atomicAdd((float *)&warp_profile[bin2], val2);
            atomicAdd((float *)&warp_weight[bin1], (float)(1. - bin2_frac));
            atomicAdd((float *)&warp_weight[bin2], (float)bin2_frac);

        }
    }
    __syncthreads();
    float sum = 0;
    float sum1 = 0;
    //fold the profile xfactor times to get the final profile
    //
    // ATT: since we do not use the mod operator to limit the number of
    // phases up to the maximum value (nbins),  we have to take into
    // account also the profile elements at index = nbins and index =
    // (nbins + 1).
    // The next procedure takes care to sum up these elements
    // respectively to element 0 and 1.
    // !! However this means that we can't elaborate data belonging to
    // !! candidates with a number of phase bins > 126, assuming that our max
    // !! number of phase bins is 128.
    //
    if (idx < nbins/xfactor) {
        for (int bin = idx; bin <= nbins + 1; bin += nbins/xfactor) {
            for (int iwarp = 0; iwarp < warp_count; iwarp ++) {
                sum +=  hist_profile[bin + iwarp * shared_phases];
                sum1 += hist_weight[bin + iwarp * shared_phases];
            }
        }
        folded[idx + start_phase] = sum;
        weight[idx + start_phase] = sum1;
    }
}


//
// void folding_worker_nosplit(float *g_in, float *folded, float *weight, int local_measures,
//                                          int candidate, int xfactor, int warp_count,
//                                          double tobs,
//                                          int max_phases, int isubint,
//                                          int threadblock_memory)
//
// g_in                 input converted data
// folded               array to store folded data
// weight               array to store weight data
// pitch_dim            the data array row length (padded with 0)
// local_measure        number of sample per channel and per subint ( <=  pitch_dim)
// candidate            the candidate number
// xfactor              the period multiplicative factor
// warp_count           the number of warp programmed on each SM
// tobs                 the observation time (to set as constant in future ??)
// max_phases           the maximum number of phase bins for the current candidate list
// isubint              the current subintegration number
// threadblock_memory   number of shared memory elements.
//
//
// Arrangement of the data into folded and weight arrays for the first
// candidate. The same organization is replicated for each other candidate
// [0, 127]: phase bins range
// ---------------------------------------------------------------------------------------
// |0|1|...|127 |0|1|...|127|...|0|1|...|127|0|1|...|127|0|1|...|127|...|0|1|...|127|....
// ---------------------------------------------------------------------------------------
// |<-band 0  ->|<-band 1 ->|...|<-band N ->|<-band 0 ->|<-band 1 ->|...|<-band N ->|
// <------------- subint 0  --------------->|<----------- subint 1  ----------------> ... <-- subint N  ->
// <------------                     candidate 0                                                 -------->
//
//
__global__ void folding_worker_nosplit(float *g_in, float *folded, float *weight,
                               int pitch_dim, int local_measures, int candidate, int xfactor,
                               int rebin, int shared_phases, int warp_count,
                               double tobs, int max_phases, int isubint, int threadblock_memory)
{

    /* threadIdx.x:      the current sample inside thread block
     * threadIdx.y       the current channel inside thread block
     * blockDim.x        the stride in samples
     * blockIdx.x:       subband index
     * blockDim.y        the stride in channels
     * gridDim.x:        number of subbands
     */
    int subband_idx  = blockIdx.x;      // the sub-band index (0, 1, ...64)
    int chan_idx = threadIdx.y;         // index of channel in current subband
                                        // (0, 1, .., n_chan_band)

    int time_idx = 0;                   // index to subint measures
    // start of phase array for current operations
    int start_phase = max_phases * subband_idx + isubint * max_phases * gridDim.x +
                      candidate * d_nsubints * max_phases *gridDim.x;
    int channel = 0;    // index of local channel
    // To be computed into CPU and put in a global tabellar form  (DONE on 24/12/2013)
    int nbins   = d_nbins[candidate];    // total number of phases of the candidate
    // physical values.
    // The extra delay in for this channel based on the DM
    // equation (see Lorimer & Kramer book)
    // time of local sample
    double time_s = 0.; // <--- WARNING!!!!!!!!!!!!
    // float has only 7 digit mantissa. for a full 600 sec datafile this is not enough!
    double phase     = 0.; // phase of single measure

    int bin1 = 0;       // profile index of the first phase bin
    int start_data = subband_idx * d_nchan_per_band * pitch_dim;
    int start_idx = 0;          //index of the first time sample of each subband
    float input_temp = 0;       //the input sample value
    float *ptr = 0;             //pointer to the data array
    extern   __shared__ float hist_profile[];   //dinamically allocated shared memory
    int idx = threadIdx.x + blockDim.x * threadIdx.y;   //thread global id
    // (idx >> 5) gives the warp number
    // 1 warp = 32 threads
    //
    // Att: if for the future GPUs 1 warp != 32, next lines has to be changed!!!
    // The warp_size is obtained by the cudaDeviceProp structure.
    float *warp_profile = hist_profile + (idx >> 5) * shared_phases;
    float *hist_weight = hist_profile + warp_count * shared_phases;
    float *warp_weight=  hist_weight + (idx >> 5) * shared_phases;

    // zero the shared memory
    for (int i = idx; i < threadblock_memory ; i += blockDim.x * blockDim.y) {
        hist_profile[i] = 0.;
        hist_weight[i] = 0.;
    }
    __syncthreads();

    // loop on time measures  (local_measures at time, nsamp/fold.nsubints)
    int nchan =  d_nchan_per_band * subband_idx; //number of the first channel of the subband
    int nsamp = local_measures * isubint;       //number of the first time sample of the
                                                //sub-integration
    float nudot_contrib = 0.;
    double t0 = -tobs * 0.5;
    for (time_idx = threadIdx.x ; time_idx < local_measures; time_idx += blockDim.x) {
        start_idx = start_data + time_idx;                      // sample position into the data array
        time_s = t0 + d_tsamp * rebin *(nsamp + time_idx);      // sample position in time
        ptr = g_in + start_idx;
        nudot_contrib = time_s * time_s * d_nudot[candidate] * 0.5;//phase shift due to acceleration
        for (chan_idx = threadIdx.y ; chan_idx < d_nchan_per_band; chan_idx += blockDim.y) {
            channel = chan_idx + nchan;                         // index of local channel
            input_temp = ptr[chan_idx * pitch_dim];
            // we compute phase
            phase = (time_s +d_dm[candidate] * d_delta_freq[channel]) *d_nu[candidate] +
                                            nudot_contrib;
            bin1 = (int)((phase - floor(phase)) * nbins);

            // ATT:
            // bin1 can be less or equal to nbins (nbins is the max number
            // of phase bins for the specific candidate), so the mod
            // operation is not necessary.
            // To avoid mod operation  (to limit the number of internal registers used) it may
            // happen that the data are stored also in the element nbins  of
            // the profile array.
            // In the computation of the final profile we will add the
            // value of element nbins to that stored in  element 0,
            //
            //
            // if at the end we sum up to the last  phase
            // (nbins) to the first one (0)
            // OSS: here frac > 0 so the integer part is equal to floor(frac)
            // input Index is computed as follows:
            // time_idx is the index of the measures (along time)
            //          of the current subint of the current channel
            // chan_idx * local_measures is the offset of start of
            //          measure of current channel from the beginning of
            //          channel block
            // The following changes only in the outer loop:
            // n_chan_band * blockIdx.x * local_measures is the offset
            //          the start of measure of current channel block
            // input_temp = g_in[time_idx + chan_idx * local_measures +
            //             n_chan_band * blockIdx.x * local_measures];

            // output
            // folded is structured as a (multidim) array:

            // max_phases  number of phases
            // weight is exactly the same.
            // start_phase = candidate * blockDim.x + subband_idx * gridDim.x

            //read and update data use atomic functions because there can be
            //collisions between threads accessing the same address
            atomicAdd((float *)&warp_weight[bin1], 1.);
            atomicAdd((float *)&warp_profile[bin1], input_temp);
        }
    }
    __syncthreads();
    float sum = 0;
    float sum1 = 0;
    //fold the profile xfactor times to get the final profile
    //
    // ATT: since we do not use the mod operator to limit the number of
    // phases up to the maximum value (nbins),  we have to take into
    // account also the profile element at index = nbins.
    // The next procedure takes care to sum up this element
    // respectively to element 0.
    // !! However this means that we can't elaborate data belonging to
    // !! candidates with a number of phase bins > 127, assuming that our max
    // !! number of phase bins is 128.
    //
    if (idx < nbins/xfactor) {
        for (int bin = idx; bin < nbins + 1; bin += nbins/xfactor) {
            for (int iwarp = 0; iwarp < warp_count; iwarp ++) {
                sum +=  hist_profile[bin + iwarp * shared_phases];
                sum1 += hist_weight[bin + iwarp * shared_phases];
            }
        }
        folded[idx + start_phase] = sum;
        weight[idx + start_phase] = sum1;
    }
}
} //cuda
} //fldo
} //cheetah
} //ska


/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/cuda/detail/FldoUtils.h"


namespace ska {
namespace cheetah {
namespace fldo {
namespace cuda {
namespace util {

/*
 *
 * void corner_turner_and_rebin(cudaDeviceProp gpu_properties, int first_bin_idx, uint64_t nsamp_subslot,
 *                              size_t nchannels, std::vector<util::CandidateRebin> &rebin,
 *                              unsigned char *d_in)
 *
 * @brief Executes the corner-turning of the input data and eventually
 * sum-up adjacent time sample (rebinning).
 *
 * @param gpu_properties        the device properties
 * @param first_bin_idx         the first rebinning index
 * @param nsamp_subslot         the number of time samples/sub-integration
 * @param ncahnnels             the number of freq. channels
 * @param rebin                 the vector with the rebinning structures
 * @param d_in                  the input data
 *
 * @return On failure throws a runtime_error exception.
 */
void corner_turner_and_rebin(cudaDeviceProp gpu_properties, int first_bin_idx, uint64_t nsamp_subslot,
                             size_t nchannels, std::vector<util::CandidateRebin> &rebin, unsigned char *d_in)
{
    //dim3 threadsPerBlock;               // number of kernel threads per block
    //dim3 blocksPerGrid;                 // number of kernel blocks for grid

    // get some properties of the device. These info are used during the
    // configuration of  GPU kernel launch.
    size_t shared_mem_per_block =gpu_properties.sharedMemPerBlock;
    int first_bin_val = 1;      // first valid rebinning value
    // set kernel launch configuration
    int tile_dimx = 32;         //number of threads in x
    int tile_dimy = 32;         //number of threads in y

    // NB:
    // The transpose operation has to be done on the original input
    // matrix. It means that even if there is no candidate with rebin = 1,
    // we have to take into account the original matrix dimension.
    // The starting input data matrix has:
    // nrows = _nchannels
    // ncols = rebin[0].pitch_dim (>= nsamp_subslot)
    // rebin[0].pitch_dim is the matrix row length returned by the call to
    // cudaMallocPitch()
    //
    int nblock_x = (nchannels + (tile_dimx - 1))/tile_dimx;
    int nblock_y  = (rebin[0].pitch_dim + (tile_dimy - 1))/tile_dimy;
    dim3 grid(nblock_x, nblock_y);
    dim3 threads(tile_dimx, tile_dimy);

    //get the size of the shared memory
    size_t shared_memory_size = tile_dimx * tile_dimy * sizeof(char);

    //check if the reserved shared memory size is less than the available
    //one
    if (shared_memory_size > shared_mem_per_block) {
        std::stringstream error_msg;
        error_msg << "Shared memory requested size is too big (requested: "
                  <<  shared_memory_size
                  << " available: "
                  << shared_mem_per_block;
        throw std::runtime_error(error_msg.str());
    }

    //get the rebinning value corresponding to theinput rebin_index
    first_bin_val = rebin[first_bin_idx].rebin;

    PANDA_LOG_DEBUG << "corner_turner: first bin index: "
                    << first_bin_idx
                    << " val: "
                    << first_bin_val
                    << " nsamp_subslot: "
                    << nsamp_subslot;
    // we handle the prebinning. (we hope it doesn't mess up!)
    // trasposePreBin kernel executes the matrix data transpose. If the
    // first binning value is > 1, it also rebins the input data.
    // The kernel is executed in a separate stream from the default one.
    // Next operations on data have to wait for the end of this
    // kernel.
#ifdef TRANSPOSE_SHFL
    if (first_bin_val > 1) {
        PANDA_LOG_DEBUG << "Call to transpose_shfl kernel: threads: ("
                        << tile_dimx << ", " << tile_dimy
                        << ") blocks: (" << nblock_x << ", " << nblock_y
                        << ") rebin: " << first_bin_val;
        transposePreBin_shfl<<<grid, threads, shared_memory_size, rebin[first_bin_idx].stream>>>
        (rebin[first_bin_idx].d_out,
         d_in,
         nchannels,
         rebin[first_bin_idx].pitch_dim,
         (int)nsamp_subslot,
         first_bin_val);
    } else {
        PANDA_LOG_DEBUG << "Call to transpose kernel: threads: ("
                        << tile_dimx << ", " << tile_dimy
                        << ") blocks: (" << nblock_x << ", " << nblock_y
                        << ") rebin: " << first_bin_val;
        transposeNoBankConflicts<<<grid,threads, shared_memory_size, rebin[first_bin_idx].stream>>>
        (rebin[first_bin_idx].d_out,
         d_in,
         nchannels,
         rebin[first_bin_idx].pitch_dim,
         (int)nsamp_subslot);
    }
#else
    PANDA_LOG_DEBUG << "Call to transposePreBin kernel: threads: ("
                    << tile_dimx << ", " << tile_dimy
                    << ") blocks: (" << nblock_x << ", " << nblock_y
                    << ") rebin: " << first_bin_val;
    transposePreBin<<<grid,threads, shared_memory_size, rebin[first_bin_idx].stream>>>
    (rebin[first_bin_idx].d_out,
     d_in,
     nchannels,
     rebin[first_bin_idx].pitch_dim,
     (int)nsamp_subslot,
     first_bin_val);
#endif
    CUDA_ERROR_CHECK(cudaGetLastError());
}
} // utils
} // namespace cuda
} // namespace fldo
} // namespace cheetah
} // namespace ska

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/fldo/test_utils/FldoTester.h"
#include "cheetah/fldo/CommonDefs.h"
#include "cheetah/fldo/Types.h"
#include "cheetah/generators/GaussianNoise.h"
#include "cheetah/generators/GaussianNoiseConfig.h"

#include "cheetah/sigproc/SigProcFileStream.h"
#include <boost/filesystem.hpp>
#include "panda/test/gtest.h"

namespace ska {
namespace cheetah {
namespace fldo {
namespace test {


template <typename TestTraits>
FldoTester<TestTraits>::FldoTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
FldoTester<TestTraits>::~FldoTester()
{
}

template<typename TestTraits>
void FldoTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void FldoTester<TestTraits>::TearDown()
{
}

/// candidate_number is the number of fake candidates produced during test
constexpr int candidate_number = 5 ;

void load_candidates(data::Scl &scl_data)
{
    typedef data::Candidate<Cpu, float> CandidateType;

    //TODO: check if float or double is needed. In this case we need to
    //update the Scl definition !!
    //open the file with candidates
    std::ifstream myfile;
    std::string line;
    double period, pdot,dm;
#ifdef DEEP_TESTING
/** This test checks the correctness of FLDO algorithm. To be used during development */
    myfile.exceptions (std::ifstream::failbit | std::ifstream::badbit );
    try {
        myfile.open (panda::test::test_file("candidates.txt"), std::fstream::in);
        std::size_t ident = 0;      // candidate identifier number
        while (!myfile.eof()) {
            getline(myfile, line);
            std::stringstream ss;
            ss << line;
            //scan line to get pulsar info
            ss >> period >> pdot >> dm;
            PANDA_LOG_DEBUG << "period: " << period << " pdot: " << pdot << " dm: " << dm;
            CandidateType::MsecTimeType period_val(period * boost::units::si::seconds);
            CandidateType::Dm dm_val(dm * data::parsecs_per_cube_cm);
            CandidateType::SecPerSecType pdot_val(pdot);
            //store values inside a candidate structure
            CandidateType candidate(period_val, pdot_val, dm_val, ident);
            ident ++;
            //add it to the scl vector
            scl_data.push_back(candidate);
        }
    }
    catch(std::ifstream::failure e) {
        std::stringstream s;
        s << "Exception opening/reading/closing file:"
          << e.what();
        PANDA_LOG_ERROR << s.str();

   }
   myfile.close();
#else // -> !DEEP_TESTING
   /** we produce few standard candidates */
   double fake_period[] = {5.94685039e-04, 7.88370079e-04, 9.82055118e-04, 1.17574016e-03, 1.12340000e-03 };
   double fake_pdot[]   = {0.0, 0.0, 0.0, 0.0, 0.0};
   double fake_dm[]     = {1.01000000e+01, 1.01000000e+01, 1.01000000e+01, 1.01000000e+01, 1.01000000e+01 };
   for (int i = 0 ; i < candidate_number ; i++ ) {
       period = fake_period[i];
       pdot   = fake_pdot[i];
       dm     = fake_dm[i];
       PANDA_LOG_DEBUG << "period: " << period << " pdot: " << pdot << " dm: " << dm;
       CandidateType::MsecTimeType period_val(period * boost::units::si::seconds);
       CandidateType::Dm dm_val(dm * data::parsecs_per_cube_cm);
       CandidateType::SecPerSecType pdot_val(pdot);
       //store values inside a candidate structure
       CandidateType candidate(period_val, pdot_val, dm_val, i);
       //add it to the scl vector
       scl_data.push_back(candidate);
    }

#endif //DEEP_TESTING
   //Print for debug the value inside the data::Scl vector
   for (data::VectorLike<std::vector<data::Scl::CandidateType>>::ConstIterator it = scl_data.begin() ;
                                                                     it != scl_data.end(); ++it) {
         PANDA_LOG << " period " << (*it).period()
                   << " pdot   " << (*it).pdot()
                   << " dm     " << (*it).dm()
                   << " ident  " << (*it).ident() ;
   }
   //sort the candidates on period basis using lambda function (ascending order)
   std::sort (scl_data.begin(), scl_data.end(), [](CandidateType first, CandidateType second) {
           return (first.period() < second.period());});
}

ALGORITHM_TYPED_TEST_P(FldoTester, test_empty_data)
{
    std::vector<std::shared_ptr<data::TimeFrequency<Cpu>>> tf_data;
    data::Scl scl_data;
    std::shared_ptr<data::Ocld> results = TypeParam::apply_algorithm(device, tf_data, scl_data);
    ASSERT_NE(nullptr, results.get());
    ASSERT_EQ(0U, results->size());
}

//
// test_empty_gaussian
// This test executes the folding operator on noisy data generated at runtime.
// This test adds information to the data about the sampling time and
// frequecies.
ALGORITHM_TYPED_TEST_P(FldoTester, test_empty_gaussian_data)
{
    typedef data::TimeFrequency<Cpu> DataType;
    typename DataType::FrequencyType delta( 1.0 * boost::units::si::mega * boost::units::si::hertz);
    typename DataType::FrequencyType start( 100.0 * boost::units::si::mega * boost::units::si::hertz);
    typedef data::Candidate<Cpu, float> CandidateType;
    //fldo::Config fldo_config;
    generators::GaussianNoiseConfig config;
    generators::GaussianNoise<typename DataType::DataType> noise(config);
    // generate some noise
    std::vector<std::shared_ptr<data::TimeFrequency<Cpu>>> tf_data;
    for(int i = 0; i < 64 ; ++i) {
        std::shared_ptr<DataType> time_frequency_data(new DataType(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(4096))); // at least one channel
        time_frequency_data->set_channel_frequencies_const_width(start, delta);
        boost::units::quantity<boost::units::si::time, double> dt(64 * boost::units::si::micro * boost::units::si::seconds);
        //set the sample interval for generated data
        time_frequency_data->sample_interval(dt);
        noise.next(*time_frequency_data);
        // appends a new element to the end of the container
        tf_data.emplace_back(time_frequency_data);
    }
    data::Scl scl_data;
    //load the list of pulsar candidates
    //
    load_candidates(scl_data);
    // init of config variables
    //fldo_config.phases(fldo::o_phases);
    //
    std::shared_ptr<data::Ocld> results = TypeParam::apply_algorithm(device, tf_data, scl_data);
    //
    PANDA_LOG_DEBUG  << " results->size = " <<  results->size();

    // check for all elements no candidates with S/N > 3
    if (0 < results->size() ) {
        for (int i = 0 ; i < candidate_number ; i++ ) {
            ASSERT_GE(3. , (*results)[0].sigma());
        }
    }
}

#ifdef DEEP_TESTING
/** This test checks the correctness of FLDO algorithm. To be used during development */
//
// test_sig_proc_file
// This test executes the folding operator on data read from a file in
// sigproc format.
// This tests gets the number of samples stored into the file and then calculates the
// optimal number of samples taking into account the number of channels,
// sub-integration and max rebin value.
// It then organizes the data in a number of chunks equal to the number of
// sub-integrations and call the folding algorithm.
ALGORITHM_TYPED_TEST_P(FldoTester, test_sigproc_file)
{
    typedef data::TimeFrequency<Cpu> DataType;
    int nsubints = 64;
    std::ifstream file_stream;
    std::stringstream ss;
    ss << "fold_2.sigproc";
    file_stream.open(panda::test::test_file(ss.str()), std::ios::binary);
    sigproc::SigProcHeader r;
    r.read(file_stream);
    PANDA_LOG_DEBUG << "number of bits: " << r.number_of_bits();
    PANDA_LOG_DEBUG << "header size: " << r.size();
    size_t nchannels = r.number_of_channels();
    size_t file_size = boost::filesystem::file_size(panda::test::test_file(ss.str()));
    uint64_t nsamples = (file_size - r.size()) * (8/r.number_of_bits());
    PANDA_LOG_DEBUG << "number of channels: " << nchannels;
    PANDA_LOG_DEBUG << "number of samples: " << nsamples;
    //-data::TimeType sampling_time =  r.sample_interval();
    //-double tobs = (nsamples/nchannels) * sampling_time.value();
    // we want the data to be a multiple also of the fldo::max_prebin value
    if ((nsamples % (nsubints * nchannels * fldo::max_prebin)) != 0) {
        nsamples = (uint64_t)((nsamples/(((float)nsubints) *nchannels* fldo::max_prebin))) * (nsubints * nchannels * fldo::max_prebin);
        //-tobs = (nsamples/nchannels) * sampling_time.value();
    }
    std::vector<std::shared_ptr<data::TimeFrequency<Cpu>>> tf_data;
    sigproc::Config config;
    //set the data file name
    config.set_sigproc_files(panda::test::test_file(ss.str()));
    // configure the chunk data dimension. The number ito specify has to be calculated as:
    // chunk_number_of_samples = number_of_sample_in_a_chunk/numer_of_channels
    // because the routine that allocates the chunk dimension (resize()) multiplies the value
    // of chunk_number_of_samples for the number of channels specified into the header
    config.set_chunk_samples(nsamples/nsubints/nchannels);
    sigproc::SigProcFileStream stream(config);
    panda::DataManager<sigproc::SigProcFileStream> chunk_manager(stream);
    stream.process();
    for (int i = 0; i < nsubints; ++i) {
        PANDA_LOG_DEBUG <<"Folding sub-integration: " << i;
        std::tuple<std::shared_ptr<DataType>> data = chunk_manager.next();
        std::shared_ptr<DataType> time_frequency_data = std::get<0>(data);
        tf_data.emplace_back(time_frequency_data);
    }
    data::Scl scl_data;
    //load the list of pulsar candidates from a file
    load_candidates(scl_data);
    std::shared_ptr<data::Ocld> results = TypeParam::apply_algorithm(device, tf_data, scl_data);
    ASSERT_EQ(0U, results->size());
}

#endif // DEEP_TESTING

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
#ifdef DEEP_TESTING
REGISTER_TYPED_TEST_CASE_P(FldoTester, test_empty_data, test_empty_gaussian_data, test_sigproc_file);
#else
REGISTER_TYPED_TEST_CASE_P(FldoTester, test_empty_data, test_empty_gaussian_data);
#endif

} // namespace test
} // namespace fldo
} // namespace cheetah
} // namespace ska

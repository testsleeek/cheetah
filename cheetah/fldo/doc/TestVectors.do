/**
* @file TestVectors.do
* @brief Descrition of test vectors employed for FLDO
* @author C.Baffa E.Giani
* @version 1.0
* @date 2016-04-28
*
**
 * @description The FLDO module is part of the PSS real time data
 * processing pipeline. So some of the tests listed in the
 * SKA-TEL.CSP.NIP.PSS-TDT-TSS document indirectly will test also this
 * module.
 *
 * In addition to the global tests, we propose few FLDO specific local
 * tests.
 *
 * Results presented here have been obtained on a Intel Core I5 @ 3.10Ghz
 * with a K40 Nvidia GPU.
 *

 *
 * @TestName Base_FLDO_test
 * @Type: module
 * @Module: FLDO
 * @Author: Michael Keith
 * @Pipeline: Pulsar Search
 * @Generation: fake -headerless -period 1.1234 -tsamp 50.0 -fch1 1550
 * -foff -0.075 -nchans 256 -tobs 15 -nbits 8 -dm 10 -snrpeak 1 > fold_2.raw
 * also present in the git repository in
 * tdt-reference-implementations/folding_optimsation/test_data
 * @Access: download
 * @Purpose: basic FLDO funtionality test
 * @Description: simulated PSS input data, 15 sec, 256 channels, 50usec
 * step, with a simulated pulsar with period 1.1234usec, DM = 10, no
 * acceleration, with a S/N = 1 at peak
 * @Format: unsigned 8bit frquency/time samples. No header
 * @Use: on a stand alone Origami, ver 1.1 June 2015, use the command
 * ./origami -c 256 -v 1 -C 1 -n 67108864 -R2
 * @Result: @{ The result should be (if git version is used):
 *
 * Warning: No candidates file found. Use default values (internally generated)
 * fold.ncand: 1 index: 0 rebin: 1 period[0]: 0.000800 candidate[0].period: 0.001123
 * fold.ncand: 1 index: 1 rebin: 2 period[1]: 0.001600 candidate[0].period: 0.001123
 * Optimizing.....
 * ------------------------------------------------------------------
 *                    Optimized values
 * ------------------------------------------------------------------
 * Candidate:    0: index:  199    S/N:  99.30 width: 0.000077 period:  0.001123400  pdot: 0  dm: 10.1000
 * ------------------------------------------------------------------
 *
 * Total exec time      :     2.01  (sec)
 * Total GPU exec time  :     0.12  (sec)
 * @}
 *

 *
 * @TestName FLDO_Performance_test
 * @Type: module
 * @Module: FLDO
 * @Author: C.Baffa, E.Giani
 * @Pipeline: Pulsar Search
 * @Generation: fake -headerless -period 2.2468 -tsamp 50.0 -fch1 1550
 * -foff -0.075 -nchans 4096 -tobs 420 -nbits 8 -dm 10 -snrpeak 10 > ska.dat
 * @Access: generate
 * @Purpose: advanced FLDO functionality test and performance assessment
 * @Description: simulated PSS input data, 420 sec, 4096 channels, 50usec
 * step, with a simulated pulsar with period 1.1234usec, DM = 10, no
 * acceleration, with a S/N = 10 at peak
 * @Format: unsigned 8bit frquency/time samples. No header
 * @Use: on a stand alone Origami, ver 1.1 June 2015, with standard
 * candidates file, use the command
 * ./origami -v0 -C128 -T 420 -c4096 -R2
 * @Result: @{ The result will be as follows (exact results will depends on
 * the random ska.dat file):
 *
 *  Line[0]:
 *  ncand: 128
 *  fold.ncand: 128 index: 0 rebin: 1 period[0]: 0.003150
 *  candidate[0].period: 0.000401
 *  fold.ncand: 128 index: 1 rebin: 2 period[1]: 0.006300
 *  candidate[8].period: 0.003514
 *  fold.ncand: 128 index: 2 rebin: 4 period[2]: 0.012600
 *  candidate[16].period: 0.006613
 *  fold.ncand: 128 index: 3 rebin: 8 period[3]: 0.025200
 *  candidate[32].period: 0.012810
 *  fold.ncand: 128 index: 4 rebin: 16 period[4]: 0.050400
 *  candidate[64].period: 0.025206
 *  Optimizing.....
 *  ------------------------------------------------------------------
 *                     Optimized values
 *  ------------------------------------------------------------------
 *  Candidate:    0: index:  351  S/N:  3.41 width: 0.000075 period:  0.000401700  pdot: 5e-08 dm:  8.766
 *  Candidate:    1: index:  375  S/N:  3.61 width: 0.000181 period:  0.001190170  pdot: 5e-08 dm: 10.76
 *  Candidate:    2: index:  284  S/N:  4.02 width: 0.000127 period:  0.001577140  pdot: 0  dm: 10.76
 *  Candidate:    3: index:  283  S/N:  4.35 width: 0.000126 period:  0.001964510  pdot: 0  dm: 10.10
 *  Candidate:    4: index:  199  S/N:307.58 width: 0.000102 period:  0.002246800  pdot: 0  dm: 10.10
 *  Candidate:    5: index:  227  S/N:  3.87 width: 0.000050 period:  0.002351580  pdot: 5e-08 dm: 10.10
 *  Candidate:    6: index:  113  S/N:  4.19 width: 0.000178 period:  0.002738450  pdot: 0  dm:  8.76
 *  Candidate:    7: index:  101  S/N:  4.36 width: 0.000101 period:  0.003125720  pdot: 5e-08 dm: 10.10
 *  ........................
 *
 *  Candidate:  127: index: 1042    S/N:  4.13 width: 0.012903 period: 0.0499989 pdot: -5e-08 dm: 10.80
 *  ------------------------------------------------------------------
 *
 *  Total exec time      :   158.01  (sec)
 *  Total GPU exec time  :   112.92  (sec)
 *  @}
 * /

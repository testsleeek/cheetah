/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_FLDO_FLDO_H
#define SKA_CHEETAH_FLDO_FLDO_H

#include "cheetah/fldo/Config.h"
#include "cheetah/fldo/Types.h"
#include "panda/ConfigurableTask.h"

namespace ska {
namespace cheetah {
namespace fldo {

/**
 * @brief
 *      The FLDO primary API
 *
 * @details
 *     the Handler should support the operator()(std::shared_ptr<data::Ocld>)
 */

template<typename Handler>
class Fldo
{
    public:
        /**
         * @brief initiate the Fldo aglorithm passing it the handler to be called on completion of the task
         */
        Fldo(ConfigType const&, Handler&);
        ~Fldo();

        /**
         * @brief generic call for an async fldo task - the handler will be called on completion utilising any device in the pool
         * @return A ResourceJob object for tracking the job status. You can use the wait() method to block until the job is complete
         */
        std::shared_ptr<panda::ResourceJob> operator()(std::vector<std::shared_ptr<data::TimeFrequency<Cpu>>>& tf_data, data::Scl const& scl_data);

    private:
        panda::ConfigurableTask<typename Config::PoolType, Handler&, TimeFrequencyType, data::Scl const&> _task;
};


} // namespace fldo
} // namespace cheetah
} // namespace ska
#include "cheetah/fldo/detail/Fldo.cpp"

#endif // SKA_CHEETAH_FLDO_FLDO_H

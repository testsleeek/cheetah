/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt/test/BeamFormerDataTraitsTest.h"
#include "cheetah/rcpt/BeamFormerPacket.h"
#include "cheetah/rcpt/BeamFormerDataTraits.h"
#include "panda/concepts/ChunkerContextDataTraitsConcept.h"
#include <vector>


namespace ska {
namespace cheetah {
namespace rcpt {
namespace test {


BeamFormerDataTraitsTest::BeamFormerDataTraitsTest()
    : ::testing::Test()
{
}

BeamFormerDataTraitsTest::~BeamFormerDataTraitsTest()
{
}

void BeamFormerDataTraitsTest::SetUp()
{
}

void BeamFormerDataTraitsTest::TearDown()
{
}

TEST_F(BeamFormerDataTraitsTest, test_packets_per_chunk)
{
    BOOST_CONCEPT_ASSERT((panda::ChunkerContextDataTraitsConcept<BeamFormerDataTraits>));

    typedef BeamFormerDataTraits::DataType DataType;
    std::size_t samples_per_packet = BeamFormerDataTraits::PacketInspector::Packet::number_of_samples();
    BeamFormerDataTraits traits;
    {
        // number_of_channels == payload size
        data::DimensionSize<data::Frequency> number_of_channels(samples_per_packet);
        data::DimensionSize<data::Time> number_of_time_samples(10);

        DataType data(number_of_time_samples, number_of_channels);
        unsigned packets_per_chunk = traits.chunk_size(data)/traits.packet_size();
        ASSERT_EQ((std::size_t)number_of_time_samples, packets_per_chunk);
    }
    {
        // number_of_channels int multiple of payload size
        data::DimensionSize<data::Frequency> number_of_channels( 2 * samples_per_packet );
        data::DimensionSize<data::Time> number_of_time_samples(2);

        DataType data(number_of_time_samples, number_of_channels);
        unsigned packets_per_chunk = traits.chunk_size(data)/traits.packet_size();
        ASSERT_EQ(number_of_time_samples * 2, packets_per_chunk);
    }
    {
        // number_of_channels < packet payload
        data::DimensionSize<data::Frequency> number_of_channels( samples_per_packet - 1 );
        data::DimensionSize<data::Time> number_of_time_samples(2);

        DataType data(number_of_time_samples, number_of_channels);
        ASSERT_EQ(number_of_time_samples * number_of_channels, traits.chunk_size(data));
        ASSERT_EQ(samples_per_packet, traits.packet_size());

        // verify realign chunk with packet boundary.
        number_of_time_samples = data::DimensionSize<data::Time>(samples_per_packet);
        DataType data2(number_of_time_samples, number_of_channels);
        unsigned packets_per_chunk = traits.chunk_size(data2)/traits.packet_size();
        ASSERT_EQ(number_of_time_samples - 1, packets_per_chunk);
    }
}

struct TestContext {
    public:
        TestContext()
            : _data(new data::TimeFrequency<Cpu>(data::DimensionSize<data::Time>(100), data::DimensionSize<data::Frequency>(100)))
        {}

        std::size_t offset() const { return 0; }
        std::size_t packet_offset() const { return 0; }
        data::TimeFrequency<Cpu>& chunk() { return *_data; }
        //TestContext& next() { _next.reset(new TestContext()); return *_next; }
        std::size_t size() const { return 2; }

        uint64_t sequence_number(BeamFormerDataTraits::PacketInspector const& packet) const { return packet.sequence_number(); }

    private:
        std::shared_ptr<data::TimeFrequency<Cpu>> _data;
        std::shared_ptr<TestContext> _next;
};

std::ostream& operator<<(std::ostream& os, TestContext const&) { return os;}

TEST_F(BeamFormerDataTraitsTest, test_deserialise)
{
    // basic compile test
    BeamFormerPacketInspector::Packet packet;
    packet.packet_type(PacketType::StokesI);
    BeamFormerDataTraits dt;
    TestContext context;
    dt.deserialise_packet(context, packet);
}


} // namespace test
} // namespace rcpt
} // namespace cheetah
} // namespace ska

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt/test/UdpStreamTest.h"
#include "cheetah/rcpt/UdpStream.h"
#include "cheetah/rcpt/Config.h"
#include "cheetah/rcpt/PacketGenerator.h"
#include "panda/DataManager.h"
#include "panda/IpAddress.h"
#include "panda/Log.h"
#include "panda/Engine.h"
#include <algorithm>
#include <deque>
#include <mutex>
#include <iostream>
#include <sstream>


namespace ska {
namespace cheetah {
namespace rcpt {
namespace test {


UdpStreamTest::UdpStreamTest()
    : ::testing::Test()
{
}

UdpStreamTest::~UdpStreamTest()
{
}

void UdpStreamTest::SetUp()
{
}

void UdpStreamTest::TearDown()
{
}

struct TestModel {
    public:
        typedef BeamFormerDataTraits::DataType DataType;

        DataType& next(DataType& data) {
            // fill data with values
            std::unique_lock<std::mutex> lk(_mutex);
            typename DataType::DataType val = 0;
            std::generate(data.begin(), data.end(), [&]{ return ++val; } );
            _data.push_back(data);
            return data;
        }

        DataType sent_data() {
            std::lock_guard<std::mutex> lk(_mutex);
            if(_data.empty()) throw panda::Error("no more sent data");
            DataType d =  _data.front();
            _data.pop_front();
            return d;
        }

        bool has_sent_data() const {
            std::lock_guard<std::mutex> lk(_mutex);
            return !_data.empty();
        }

    private:
        mutable std::mutex _mutex;
        std::deque<DataType> _data;
};

static
std::string write_data(BeamFormerDataTraits::DataType const& data)
{
    std::stringstream ss;
    ss << "(";
    for(auto d : data) {
        ss << (unsigned)d << ",";
    }
    ss << ")";
    return ss.str();
}

template<typename Packet>
void test_udp_packets_stream_data_consistency(std::size_t number_of_channels)
{
    typedef ska::panda::Connection<ska::panda::ConnectionTraits<ska::panda::Udp>> ConnectionType;

    panda::IpAddress address(0, "127.0.0.1");
    boost::asio::ip::udp::endpoint local_endpoint = address.end_point<boost::asio::ip::udp::endpoint>();

    rcpt::Config config;
    config.samples_per_chunk(1); // this is the number of time samples
    ASSERT_EQ(1U, config.samples_per_chunk());


    SCOPED_TRACE(number_of_channels);
    SCOPED_TRACE("number_of_channels per time sample:");
    PANDA_LOG_DEBUG << "number_of_channles per time sample=" << number_of_channels;
    config.number_of_channels(number_of_channels);
    config.remote_end_point(local_endpoint);
    ASSERT_EQ(number_of_channels, config.number_of_channels());

    rcpt::UdpStream stream(config);
    ska::panda::DataManager<UdpStream> dm(stream);
    auto endpoint = stream.local_end_point();

    // Start a UDP stream
    TestModel model;
    typedef PacketGenerator<TestModel> GeneratorType;
    GeneratorType generator(model, config.number_of_channels(), config.samples_per_chunk());
    panda::Engine& engine = config.engine();
    ConnectionType connection(engine); // to send packets
    connection.set_remote_end_point(endpoint);

    // construct a data set that matches the specified config to use as a reference
    BeamFormerDataTraits::DataType data_ref(data::DimensionSize<data::Time>(config.samples_per_chunk())
                                           , data::DimensionSize<data::Frequency>(config.number_of_channels()));
    BeamFormerDataTraits traits;

    // test data throughput
    unsigned delta = 0U;
    unsigned received_count=0U;
    engine.poll(); // ensure the DataManager has completed all init jobs before we start testing throughput

    // send enough packets to fill a single chunk
    std::size_t packet_tot = delta;
    auto chunk_size = traits.chunk_size(data_ref);
    unsigned packet_count = 0U;
    // We send more packet than we really need as some OS's will drop the first packet if there is no ARP entry in the
    // cache (https://stackoverflow.com/questions/11812731/first-udp-message-to-a-specific-remote-ip-gets-lost) and will only
    // queue one more until resolved
    while(packet_tot < 2 * chunk_size )
    {
        ++packet_count;
        connection.send(generator.next(), [&](ska::panda::Error e){
                ASSERT_FALSE(e);
                ++received_count;
                });
        engine.poll_one();
        packet_tot += traits.packet_size();
    }
    delta = packet_tot - chunk_size;
    PANDA_LOG << "sent " <<  packet_count << " packets";

    unsigned max_attempts=1000;
    while(received_count < packet_count) {
        engine.poll_one();
        if(--max_attempts == 0) FAIL() << "time out: expected packets not received";
    }

    auto data_out_ptr = std::get<0>(dm.next()); // keep it in context
    auto const& data_out = *data_out_ptr;
    ASSERT_NE(nullptr, &*data_out.begin());

    // find where the stream has started and align
    auto model_data = model.sent_data();
    auto sent_it = model_data.cbegin();
    auto it = data_out.cbegin();

    // we don't know which packet would of been realigned toaso iterate through until we find one that matches
    unsigned count=0;
    unsigned packet_num=0;
    do {
        if(*sent_it != *it) {
            // TODO should make info easier to interpret by writing from the packe size offset in the model data
            //      rather than the whole data chunk
            PANDA_LOG_ERROR << "did not match packet " << packet_num << "\n"
                      << "got " << (int)*it << " sent:" << (int)*sent_it << " at position " << count
                      << "\n\tpacket samples (" << Packet::number_of_samples() << ")\n"
                      << "\tmodel data size(" << model_data.data_size() << ")\n"
                      << write_data(model_data) << "\n"
                      << "\tactual size=" << data_out.data_size()<< "( " << &data_out << ")\n"
                      << write_data(data_out);

            // increase by a packets worth of data
            for(unsigned i=count; i < Packet::number_of_samples(); ++i)
            {
                if(++sent_it == model_data.end())
                {
                    ASSERT_TRUE(model.has_sent_data()) << "unable to match data to any packets sent after checking " << packet_num << " packets." << " i=" << i;
                    model_data=model.sent_data();
                    sent_it=model_data.cbegin();
                }
            }
            ++packet_num;
            it = data_out.cbegin();
            count = 0;
        }
        else {
            if(++it == data_out.cend()) break;
            ++count;
            if(++sent_it == model_data.end())
            {
                ASSERT_TRUE(model.has_sent_data()) << "unable to match data to any packets sent after checking " << packet_num << " packets.";
                model_data=model.sent_data();
                sent_it=model_data.cbegin();
            }
        }
    } while(true);
}

TEST_F(UdpStreamTest, test_udp_packets_stream_data_consistency_channels_size_equal_packet_size)
{
    typedef BeamFormerPacketMid Packet;
    test_udp_packets_stream_data_consistency<Packet>(Packet::number_of_samples());
}

TEST_F(UdpStreamTest, test_udp_packets_stream_data_consistency_channels_size_greater_packet_size)
{
    typedef BeamFormerPacketMid Packet;
    test_udp_packets_stream_data_consistency<Packet>(Packet::number_of_samples() + 1);
}

TEST_F(UdpStreamTest, test_udp_packets_stream_data_consistency_channels_size_less_packet_size)
{
    typedef BeamFormerPacketMid Packet;
    test_udp_packets_stream_data_consistency<Packet>(Packet::number_of_samples() - 1);
}


} // namespace test
} // namespace rcpt
} // namespace cheetah
} // namespace ska

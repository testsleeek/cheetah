/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/rcpt/BeamFormerDataTraits.h"
#include "cheetah/rcpt/BeamFormerPacket.h"
#include <iostream>
#include <cstdlib>
#include <type_traits>


namespace ska {
namespace cheetah {
namespace rcpt {


BeamFormerDataTraits::BeamFormerDataTraits()
{
}

BeamFormerDataTraits::~BeamFormerDataTraits()
{
}

uint64_t BeamFormerDataTraits::sequence_number(PacketInspector const& packet)
{
    return packet.packet().packet_count(); // * _scale_factor;
}

std::size_t BeamFormerDataTraits::chunk_size(DataType const& chunk)
{
    return chunk.number_of_spectra() * chunk.number_of_channels();
}

std::size_t BeamFormerDataTraits::packet_size()
{
    return PacketInspector::Packet::number_of_samples();
}

bool BeamFormerDataTraits::align_packet(PacketInspector const& inspector)
{
    return inspector.packet().first_channel_number() == 0;
}

void BeamFormerDataTraits::packet_stats(uint64_t packets_missing, uint64_t packets_expected)
{
    if(packets_missing > 0)
    {
        PANDA_LOG_WARN << "missing packets: " << packets_missing << " in " << packets_expected;
    }
}

} // namespace rcpt
} // namespace cheetah
} // namespace ska

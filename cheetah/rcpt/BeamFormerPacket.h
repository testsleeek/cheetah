/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DATA_BEAMFORMERPACKET_H
#define SKA_CHEETAH_DATA_BEAMFORMERPACKET_H

#include "cheetah/rcpt/Sample.h"
#include <boost/endian/arithmetic.hpp>
#include <cstdlib>
#include <cstdint>

namespace ska {
namespace cheetah {
namespace rcpt {

/**
 * @brief
 *   Interface to packing/unpacking rcpt from the BeamFormer rcpt stream UDP packet
 *
 * @details
 * 
 */

enum class PacketType { StokesI = 1U, StokesQ, StokesRe, StokesIm, AntennaCount };

template<std::size_t PayloadSize=8192>
class BeamFormerPacket
{
    private:
        static constexpr std::size_t _payload_size = PayloadSize;

    public:
        BeamFormerPacket();
        ~BeamFormerPacket();


        /**
         * @brief the total size of the udp packets header
         */
        constexpr static std::size_t header_size();

        /**
         * @brief the total size of the udp packets footer
         */
        constexpr static std::size_t footer_size();

        /**
         * @brief the total size in bytes of the channel rcpt
         */
        constexpr static std::size_t payload_size();

        /**
         * @brief the total number of samples in the rcpt payload 
         */
        constexpr static std::size_t number_of_samples();

        /**
         * @brief the total number of frequencey channels in the rcpt payload 
         */
        static std::size_t number_of_channels();

        /**
         * @brief the total size of the packet (header + payload + footer)
         */
        constexpr static std::size_t size();

        /**
         * @brief set the counter in the header
         * @details this is stored as a 48bit unsigned integer
         *          anything larger than this passed will have the 48bit modulo applied modulo
         */
        void packet_count(uint64_t);

        /**
         * @brief set the type of packet
         */
        void packet_type(PacketType const);

        /**
         * @brief retunr the packet type
         */
        PacketType packet_type() const;

        /**
         * @brief get the counter info from header
         */
        uint64_t packet_count() const;

        /**
         * @brief insert a sample
         */
        void insert(std::size_t sample_number, Sample s);
        
        /**
         * @brief return the named sample
         */
        Sample const& sample(std::size_t sample) const;

        const Sample* begin() const;
        const Sample* end() const;

        /**
         * @brief return the maximum value the packet_count can take
         */
        static constexpr uint64_t max_sequence_number();

        /**
         * @brief the number of the fitst channel in the packet
         */
        uint16_t first_channel_number() const;
        void first_channel_number(uint16_t number);

    private:
        struct Header {
            boost::endian::little_uint64_t _counter;                 // little endian counter of 8 bytes
            boost::endian::little_uint32_t _timestamp_seconds;       // seconds counter of 4 bytes
            boost::endian::little_uint32_t _timestamp_nanoseconds;   // nanaoseconds counter of 4 bytes
            boost::endian::little_uint16_t _beam_number;
            boost::endian::little_uint16_t _first_channel_number;
            boost::endian::little_uint16_t _number_of_channels;
            uint8_t _beamformer_version;
            boost::endian::little_uint16_t _sample_count_type_5;
            uint8_t _integration_time;                               // in number of samples
            uint8_t _band_number;
            uint8_t _packet_type;                                    // 0 = invalid,, 1-4 stokes parameters, 5 = antennna count
            uint8_t _reserved[4];
        } _header;

        Sample _data[_payload_size/sizeof(Sample)];

/*
        struct Footer {
        }; // _footer;
*/
        static const std::size_t _number_of_samples= _payload_size/sizeof(Sample);
        static const std::size_t _size = _payload_size + sizeof(Header);
        //static const std::size_t _size = _payload_size + sizeof(Header) + sizeof(Footer); // use if Footer exists
};

static constexpr unsigned low_payload_size = 8192;
static constexpr unsigned mid_payload_size = 4096;
typedef BeamFormerPacket<low_payload_size> BeamFormerPacketLow;
typedef BeamFormerPacket<mid_payload_size> BeamFormerPacketMid;


} // namespace rcpt
} // namespace cheetah
} // namespace ska
#include "cheetah/rcpt/detail/BeamFormerPacket.cpp"

#endif // SKA_CHEETAH_DATA_BEAMFORMERPACKET_H 

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/sift/simple_sift/Sift.h"
#include "panda/Log.h"
#include <algorithm>

namespace ska {
namespace cheetah {
namespace sift {
namespace simple_sift {

Sift::Sift(Config const& config, sift::Config const& algo_config)
    : utils::AlgorithmBase<Config, sift::Config>(config, algo_config)
{
}

Sift::~Sift()
{
}

// A comparator to sort a list of candidates by sigma, from highest to lowest
static bool sort_vector_by_sigma_asc(data::Ccl::CandidateType const& candidate_one, data::Ccl::CandidateType const& candidate_two) {
    return (candidate_one.sigma() > candidate_two.sigma());
}

std::shared_ptr<data::Scl> Sift::process(ResourceType& /*cpu*/,
    data::Ccl& ccl)
{

    double candidate_period, test_period, candidate_dm, test_dm, period_ratio;
    std::vector<bool> identified_as_harmonic(ccl.size(), {false});
    std::vector<double> harmonic_ratio;
    std::size_t candidate_index, test_candidate_index;
    data::Ccl::CandidateType candidate, test_candidate;

    // Shared pointer to structure in which to store sifted periodicity candidates
    auto scl = std::make_shared<data::Scl>();

    // Sort the list by S/N, highest to lowest
    std::sort(ccl.begin(), ccl.end(), sort_vector_by_sigma_asc);

    // Calculate harmonic ratios (1/1, 2/2, 3/2, 4/2, ..., 8/7, 8/8)
    for (std::size_t top = 1; top <= _algo_config.num_candidate_harmonics(); top++) {
      for (std::size_t bottom = 1; bottom <= top; bottom++) {
        harmonic_ratio.push_back((double) top/bottom);
      }
    }

    // Sort the vector of ratios
    std::sort(harmonic_ratio.begin(), harmonic_ratio.end());

    // Remove duplicate ratios and resize the vector
    auto iter = unique(harmonic_ratio.begin(), harmonic_ratio.end());
    harmonic_ratio.resize(distance(harmonic_ratio.begin(), iter));

    // Loop until all candidates have been dealt with
    while (true) {

      // Find the candidate with the highest sigma that has not been sifted
      candidate_index = std::find(identified_as_harmonic.begin(), identified_as_harmonic.end(), 0) - identified_as_harmonic.begin();
      if (candidate_index >= identified_as_harmonic.size()) { // If the index is greater than the vector size, it means it has not found any zeros
        break;
      }

      PANDA_LOG_DEBUG << "candidate_index = " << candidate_index;

      // Start with the highest S/N candidate and check to see if any of the other candidates are related
      candidate = ccl[candidate_index];
      candidate_period = boost::units::quantity_cast<double>(candidate.period());
      candidate_dm = boost::units::quantity_cast<double>(candidate.dm());

      PANDA_LOG_DEBUG << "candidate_period = " << candidate_period << ", candidate_dm = " << candidate_dm;

      // Put this candidate into the sifted candidate list
      scl->push_back(candidate);

      // Mark that this candidate has been added to the scl
      identified_as_harmonic[candidate_index] = 1;

      // Go through every remaining signal in the list to check for related signals
      for (test_candidate_index = candidate_index + 1; test_candidate_index < ccl.size(); test_candidate_index++) {

        if (identified_as_harmonic[test_candidate_index]) {
          continue;
        } else {

          PANDA_LOG_DEBUG << "test_cand_index = " << test_candidate_index;

          test_candidate = ccl[test_candidate_index];
          test_period = boost::units::quantity_cast<double>(test_candidate.period());
          test_dm = boost::units::quantity_cast<double>(test_candidate.dm());

          PANDA_LOG_DEBUG << "test_period = " << test_period << ", test_dm = " << test_dm;

          // Since all of the ratios generated to test for harmonics are greater than one, make sure the ratio of the periods is greater than one
          if (candidate_period >= test_period) {
            period_ratio = candidate_period/test_period;
          } else {
            period_ratio = test_period/candidate_period;
          }

          // Go through each ratio and test whether this period is some harmonic of the candidate
          for (auto iter = harmonic_ratio.begin(); iter != harmonic_ratio.end(); ++iter) {
            if (std::abs(period_ratio - *iter) < (_algo_config.match_factor() * *iter)) {
              // Test whether this DM is within +-10 of the DM of the candidate
              if (std::abs(candidate_dm - test_dm) < 10) {
                // Mark this test_candidate as a harmonic
                identified_as_harmonic[test_candidate_index] = 2;
                PANDA_LOG_DEBUG << "Harmonic found!";
                break;
              }
            }
          }

        }

      }

    }

    return scl;

}

} //simple_sift
} //sift
} //cheetah
} //ska

/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_SIFT_SIFT_H
#define SKA_CHEETAH_SIFT_SIFT_H

#include "cheetah/sift/Config.h"
#include "cheetah/sift/simple_sift/Sift.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/Candidate.h"
#include "cheetah/data/Scl.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/utils/Architectures.h"

#include "panda/AlgorithmTuple.h"

namespace ska {
namespace cheetah {
namespace sift {

/**
 * @brief	Top level sync + async mixed interface for Sift module
 *
 * @details	
 *      SIFT will match together candidates that are suspected to belong to the same original signal.
 *      For instance, detections of the same periodicity at different values of dispersion measure or acceleration, 
 *      or detections that are harmonics of other signals are merged. 
 *      The goal is to produce the sifted candidate list (SCL) containing one candidate per incoming signal.
 *
 * @tparam Handler some function of the form operator()(std::shared_ptr<data::Scl>)
 */
template<typename Handler>
class Sift
{
    private:
        typedef panda::AlgorithmTuple<simple_sift::Sift> Implementations;

    public:

        /**
         * @brief      Construct a new Sift instance
         *
         * @param      config  The algorithm configuration
         */
        Sift(Config const& config, Handler);
        Sift(Sift const&) = delete;
        Sift(Sift&&) = default;
        ~Sift();

        /**
         * @brief	Sift candidates in a Ccl based on their periods and DMs.
         *
         * @detail	This is a forwarding interface that will forward the call to the
         *		relevant implementation based on the provided arguments.
         *
         * @param	resource   A panda PoolResource object on which to process
         * @param	input      The input Ccl
         *
         * @tparam	Arch       A processing architecture
         *
         * @return	A shared pointer to a list of sifted candidates
         */
        template <typename Arch>
        std::shared_ptr<data::Scl> operator()(
                                    panda::PoolResource<Arch>& resource,
                                    data::Ccl& input);

        /**
         * @brief	Async call to Sift
         *
         * @param	input       A Ccl list to be sifted
         */
        void operator()(std::shared_ptr<data::Ccl> const& input);


    private:
        Config const& _config;
        Implementations _implementations;
        Handler _handler;
};

} // namespace sift
} // namespace cheetah
} // namespace ska

#include "cheetah/sift/detail/Sift.cpp"

#endif // SKA_CHEETAH_SIFT_SIFT_H
